`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.03.2019 15:48:49
// Design Name: 
// Module Name: IncrementarOperando
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module IncrementardecrementarOperando#(parameter nbits=4)(
input logic [nbits-1:0] a,
input logic [nbits-1:0] b,
input logic ALUFLAG1,
output logic [nbits-1:0] y1,y2,
output logic c1,c2
    );
    always_comb
    begin
    if (ALUFLAG1==0)
       begin
       c1= (|a==1)? 1:0; 
       y1=(a-1);
       c2= (&a==1)? 1:0;
       y2=(a+1);
       end
    else
        begin
        c1= (|b==1)? 1:0; 
        y1=(b-1);
        c2= (&b==1)? 1:0; 
        y2=(b+1);
        end
    end
endmodule
