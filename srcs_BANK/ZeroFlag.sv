`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 19.03.2019 13:53:46
// Design Name: 
// Module Name: ZeroFlag
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ZeroFlag#(parameter inputbits=4)(
    output  wire Flagzero,
    input   wire [inputbits-1:0] S,
    input   wire Cout,
    input   wire Sign
    
    );
    assign Flagzero = ~|(S)&~(~(Sign)&~(Cout));
endmodule
