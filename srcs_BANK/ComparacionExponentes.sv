`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Johan Chaves Zamora
// 
// Create Date: 02.04.2019 15:24:28
// Design Name: 
// Module Name: ComparacionExponentes
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ComparacionExponentes(
    input logic [7:0] NumAex,
    input logic [7:0] NumBex,
    output logic [7:0] Dezplazamiento,
    output logic selectdez
    );
    always_comb
    begin
    if( NumAex <NumBex)
    begin
        Dezplazamiento=NumBex-NumAex;
        selectdez=0;
    end else
        begin 
        Dezplazamiento=NumAex-NumBex;
        selectdez=1;
        end
    if(NumAex==NumBex)
        begin
        selectdez=0;
        Dezplazamiento=7'b0000000;
        end
    end
endmodule
