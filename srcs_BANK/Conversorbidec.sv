`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 17.03.2019 23:10:33
// Design Name: 
// Module Name: Conversorbidec
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Conversorbidec#(parameter nbits=4)(
input logic [nbits-1:0] a,
output integer b
    );
    initial
    begin
    b=a;
    end
endmodule
