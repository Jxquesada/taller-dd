`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.03.2019 17:11:54
// Design Name: 
// Module Name: LogicalFunc
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module LogicalFunc#(parameter nbits=4)(input logic [nbits-1:0] a, b,
input logic ALUFLAG1,
output logic [nbits-1:0] y1, y2,
y3, y4);
    logic  [nbits-1:0] y41,y42;
    assign y1 = a & b; // AND
    assign y2 = a | b; // OR
    assign y3 = a ^ b; // XOR  
    always_comb
    begin
    if(ALUFLAG1==1)
     begin
     y4=~a;// NOT A 
     end
    else
      begin
      y4=~b;// NOT B
      end
    end
endmodule

