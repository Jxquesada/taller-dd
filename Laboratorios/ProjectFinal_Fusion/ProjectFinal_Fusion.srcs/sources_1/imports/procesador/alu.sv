`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.06.2019 19:27:31
// Design Name: 
// Module Name: alu
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module alu #(parameter ancho = 32)(
input [ancho-1:0]a,
    input [ancho-1:0]b,
    input alu_flag_in,
    input [2:0] opcode,
    output logic  [3:0] flags,
    output logic [ancho-1:0] result
        );
logic [ancho-1:0] y0,y1;
logic c0,c1;
logic zero0,zero1;
logic negativo0,negativo1;
logic overflow0,overflow1;
logic c,zero,negativo,overflow;      

assign flags[0]=overflow;
assign flags[1]=c;
assign flags[2]=zero;
assign flags[3]=negativo;  

m_aritmetico #(ancho) modo_a (a,b,opcode[0],opcode[1],alu_flag_in,y0,negativo0,c0,zero0,overflow0);
m_logico  #(ancho) modo_b (a,b,opcode[0],opcode[1],alu_flag_in,y1,negativo1,zero1,c1,overflow1);
mux2 #(ancho) mux_salidas (y0,y1,opcode[2],result);

mux2 #(1) mux_c (c0,c1,opcode[2],c);
mux2 #(1) mux_zero (zero0,zero1,opcode[2],zero);
mux2 #(1) mux_negativo (negativo0,negativo1,opcode[2],negativo);
mux2 #(1) mux_overflow (overflow0,overflow1,opcode[2],overflow);
endmodule

module m_aritmetico#(parameter ancho = 32)
(
    input [ancho-1:0] a,
    input [ancho-1:0] b,
    input op_code1,op_code2,alu_flag_in,
    output [ancho-1:0] y,
    output logic negativo,
    output logic c,
    output logic zero,
    output logic overflow
    );

logic select1,select2;
logic sal_cin,sal_operando;
logic [ancho-1:0] sal_ab1;
logic [ancho-1:0] sal_ab2; 
logic signo_op1;
logic signo_op2;

assign signo_op1 = a[ancho-1];
assign signo_op2 = b[ancho-1]; 


deco_cin  flag_in (op_code2,alu_flag_in,sal_cin,sal_operando);

mux2 #(ancho) mux_b (a,b,sal_operando,sal_ab1);
mux2 #(ancho) mux_operando (a,sal_ab1,select2,sal_ab2);
logic sign;
logic ent1=1'b0;
logic ent2=1'b1;
mux2 #(1)  mux_habilitador (ent1,ent2,select1,select2);
assign select1=op_code2;
logic [ancho-1:0] b_compl2;

compl_2 #(ancho) b_complemento (b,b_compl2);


logic [ancho-1:0] operando_compl2;
logic [ancho-1:0] salida_select1;
logic msb;

mux2  #(ancho) operaciones_aritemeticas (b,b_compl2,op_code1,salida_select1);

adder_cmp #(ancho) sumador (sal_ab2,salida_select1,sal_cin,msb,y);
 
assign c = msb;
assign sign = y[ancho-1];
assign zero = (y==3'b000) ? 1'b1 :1'b0;
assign negativo = (sign==1'b1) ? 1'b1 : 1'b0;

logic  [2:0] caso_overflow_sum;
logic [2:0] caso_overflow_rest;
logic overflow_sum;
logic overflow_rest;
assign caso_overflow_sum = (a[ancho-1]~^b[ancho-1]~^0)&(a[ancho-1]^y[ancho-1]);
assign caso_overflow_rest = (a[ancho-1]~^b[ancho-1]~^1)&(a[ancho-1]^y[ancho-1]);
assign overflow_sum=caso_overflow_sum;
assign overflow_rest=caso_overflow_rest;
mux2  #(1) overflows (overflow_sum,overflow_rest,op_code1,overflow);
endmodule

module m_logico#(parameter ancho = 32)(
    input [ancho-1:0]a,
    input [ancho-1:0]b,
    input wire op_code1,op_code2,
    input wire alu_flag_in,
    output [ancho-1:0]y,
    output logic negativo,zero,c,overflow
    );  
    
 logic select1,select3,sal_enable_op,sal_enable_xor ;
 logic [1:0] select2;
 deco__flag_in  flag_in (alu_flag_in,op_code2,op_code1,sal_enable_op,sal_enable_xor);
 assign select1 = sal_enable_op;
 assign select2 = {op_code2,op_code1};
 logic [ancho-1:0] a_negado;  
 logic [ancho-1:0] b_negado;
 logic [ancho-1:0] salida_a,salida_b,salidas;
 logic  [ancho-1:0] op_nand,op_and,op_xor,op_or;//or agregado
 logic [1:0] select4;
assign a_negado = ~(a);
assign b_negado = ~(b);


 logic sign;
 logic [ancho-1:0] salida_op_negado;
 
//mux_2a1parametrizable #(3) mux_a (a,a_negado,select1,salida_a);
//mux_2a1parametrizable #(3) mux_b (b,b_negado,select1,salida_b);
//mux_2a1parametrizable #(3) mux_negados (a_negado,b_negado,alu_flag_in,salida_op_negado);
assign op_nand = ~ (salida_a & salida_b);
assign op_and = a & b;
assign op_or=a | b;//operacion agregada or
//mux_4a1parametrizable #(3) mux_operaciones (op_nand,op_and,salida_op_negado,3'b000,select2,salidas);
assign op_xor = a ^ b;
assign select3 = sal_enable_xor;
assign sign = y[ancho-1];

mux4 #(ancho) mux_operaciones (op_xor,op_or,op_and,32'b0,select2,y);

assign negativo = (sign==1'b1) ? 1'b1 : 1'b0;
assign zero = (y==32'b0) ? 1'b1 :1'b0;
assign c = 1'b0;
assign overflow = 1'b0;
endmodule

module deco_cin
(
    input logic a,b,
    output logic out1,
    output logic out2
    );
always_comb
begin
case ({a,b})
0 : begin out1 = 1'b0; out2 = 1'b1; end
1 : begin out1 = 1'b1; out2 = 1'b1; end
2 : begin out1 = 1'b0; out2 = 1'b0; end
3 : begin out1 = 1'b0; out2 = 1'b1; end
endcase 
end  
endmodule

module compl_2#(parameter ancho=1)(
    input logic [ancho-1:0] num,
    output logic [ancho-1:0] num_compl2
    );

logic [ancho-1:0]compl_1; 

assign compl_1=~num;
assign num_compl2 = compl_1+1'b1;
endmodule

module adder_cmp
#(parameter ancho = 1)
  (
   input [ancho-1:0] a,
   input [ancho-1:0] b,
   input cin,
   output cout,
   output [ancho-1:0]  f 
   );
     
  wire [ancho:0]     w_C;
  wire [ancho-1:0]   w_G, w_P, w_SUM;
  //wire w_cout;
 
  // Create the Full Adders
  genvar             ii;
  generate
    for (ii=0; ii<ancho; ii=ii+1) 
      begin
        bit_adder full_adder_inst
            ( 
              .i_bit1(a[ii]),
              .i_bit2(b[ii]),
              .i_carry(w_C[ii]),
              .o_sum(w_SUM[ii]),
              .o_carry()
              );
      end
  endgenerate
 
  // Create the Generate (G) Terms:  Gi=Ai*Bi
  // Create the Propagate Terms: Pi=Ai+Bi
  // Create the Carry Terms:
  genvar             jj;
  generate
    for (jj=0; jj<ancho; jj=jj+1) 
      begin
        assign w_G[jj]   = a[jj] & b[jj];
        assign w_P[jj]   = a[jj] | b[jj];
        assign w_C[jj+1] = w_G[jj] | (w_P[jj] & w_C[jj]);
      end
  endgenerate
   
  assign w_C[0] = cin; // no carry input on first adder
 assign cout = w_C[ancho];
  assign f = w_SUM;   // Verilog Concatenation   
  //assign cout =w_cout;
endmodule // carry_lookahead_adder

module deco__flag_in(
    input logic flag_in,
    input opcode_2,
    input opcode_1,
    output logic out1,
    output  logic out2
    );
always_comb
begin 
case ({flag_in,opcode_2,opcode_1})
0 : begin out1 = 1'b0; out2 = 1'b1;end  
1 : begin out1 = 1'b0; out2 = 1'b0;end
2 : begin out1 = 1'b1; out2 = 1'b0;end
3 : begin out1 = 1'bz; out2 = 1'bz;end
4 : begin out1 = 1'b1; out2 = 1'b0;end
5 : begin out1 = 1'bz; out2 = 1'bz;end
6 : begin out1 = 1'b0; out2 = 1'b0;end
7 : begin out1 = 1'bz; out2 = 1'bz;end
endcase
end   
endmodule
module  mux4 #(parameter ancho = 2 ) (
    input logic [ancho-1:0] a,b,c,d,
    input logic [1:0] select,
    output logic [ancho-1:0]data_selected
    );  

    always_comb 
    begin
    case(select)
    0 : data_selected = a ;
    1 : data_selected = b ;
    2 : data_selected = c ;
    3 : data_selected = d ;
    endcase
    end 
endmodule
