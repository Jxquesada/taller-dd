
//module testbench();

//  logic        clk;
//  logic        reset;

//  logic [31:0] WriteData, DataAdr,valor_pc,instr_actual,Rd1,Rd2;
//  logic        MemWrite;

//  top dut(clk, reset, WriteData, DataAdr,valor_pc,instr_actual,Rd1,Rd2,MemWrite);
  
//  initial
//    begin
//      reset <= 1; # 22; reset <= 0;
//    end

//  always
//    begin
//      clk <= 1; # 5; clk <= 0; # 5;
//    end
//  always @(negedge clk)
//    begin
//      if(MemWrite) begin
//        if(DataAdr === 100) begin
//          $display("Simulation succeeded");
//          $stop;
//        end else if (DataAdr !== 96) begin
//          $display("Simulation failed");
//          $stop;
//        end
//      end
//    end
//endmodule

module top(input  logic        clk, reset,IRQ,//se agrego IRQ para interrupci�n
            input logic [31:0] ReadData, 
           output logic [31:0] WriteData, DataAdr,
           output logic        MemWrite);

  logic [31:0] PC, Instr;
  
  arm arm(clk, reset,IRQ, PC, Instr, MemWrite, DataAdr, 
          WriteData,SrcA,SrcB,ReadData);
  imem imem(PC, Instr);
  dmem dmem(clk, MemWrite,IRQ, DataAdr, WriteData, ReadData);
  
  
 
endmodule

module dmem(input  logic        clk, we,IRQ,
            input  logic [31:0] a1, wd1,//se modifico nombre con un 1 para valores en tercerestado en interrupciones
            output logic [31:0] rd1);

  logic [31:0] RAM[63:0];
  logic [31:0] a,wd,rd;
  
  initial 
  begin
   RAM[0]=0;RAM[1]=0;RAM[2]=0;RAM[3]=0;RAM[4]=0;RAM[5]=0;RAM[6]=0;RAM[7]=0;RAM[8]=0;RAM[9]=0; 
   RAM[10]=0;RAM[11]=0;RAM[12]=0;RAM[13]=0;RAM[14]=0;RAM[15]=0;RAM[16]=0;RAM[17]=0;RAM[18]=0;RAM[19]=0;
   RAM[20]=0;RAM[21]=0;RAM[22]=0;RAM[23]=0;RAM[24]=0;RAM[25]=0;RAM[26]=0;RAM[27]=0;RAM[28]=0;RAM[29]=0;
   RAM[30]=0;RAM[31]=0;RAM[32]=0;RAM[33]=0;RAM[34]=0;RAM[35]=0;RAM[36]=0;RAM[37]=0;RAM[38]=0;RAM[39]=0; 
   RAM[40]=0;RAM[41]=0;RAM[42]=0;RAM[43]=0;RAM[44]=0;RAM[45]=0;RAM[46]=0;RAM[47]=0;RAM[48]=0;RAM[49]=0;
   RAM[50]=0;RAM[51]=0;RAM[52]=0;RAM[53]=0;RAM[54]=0;RAM[55]=0;RAM[56]=0;RAM[57]=0;RAM[58]=0;RAM[59]=0; 
   RAM[60]=0;RAM[61]=0;RAM[62]=0;RAM[63]=0;               
  end 

  assign rd = RAM[a[31:2]]; 

  always_ff @(posedge clk)
    if (we) RAM[a[31:2]] <= wd;
    else 
         RAM[a[31:0]]<=0;
         
  //agregado para interrupciones, tercer estado
  assign a1=(IRQ) ? 'bz : a;
  assign wd1=(IRQ) ? 'bz : wd;
  assign rd1=(IRQ) ? 'bz : rd;
endmodule

module imem(input  logic [31:0] a,
            output logic [31:0] rd);

  logic [31:0] RAM[63:0];

  initial
      $readmemh("mem_file.mem",RAM);

  assign rd = RAM[a[31:2]];
endmodule

module arm(input  logic        clk, reset,IRQ,
           output logic [31:0] PC,
           input  logic [31:0] Instr,
           output logic        MemWrite,
           output logic [31:0] ALUResult, WriteData,SrcA,SrcB,
           input  logic [31:0] ReadData);

  logic [3:0] ALUFlags;
  logic       RegWrite, 
              ALUSrc, MemtoReg,Mov,mov_reg,Shift;
  logic [1:0] RegSrc, ImmSrc, PCSrc;
  logic [2:0] ALUControl;

  controller c(clk, reset,IRQ, Instr[31:20],Instr[15:12],Instr[11:4],ALUFlags, 
               RegSrc, RegWrite, ImmSrc, 
               ALUSrc,Mov,mov_reg,Shift,ALUControl,
               MemWrite, MemtoReg, PCSrc);
  datapath dp(clk, reset, 
              RegSrc, RegWrite,Shift, ImmSrc,
              ALUSrc, ALUControl,
              MemtoReg, PCSrc,Mov,mov_reg,
              ALUFlags, PC, Instr,
              ALUResult, WriteData,SrcA,SrcB,ReadData);
endmodule

module controller(input  logic         clk, reset,IRQ,
	              input  logic [31:20] Instr,
	              input  logic [15:12] rd,
	              input  logic [11:4] Instr2, 
                  input  logic [3:0]   ALUFlags,
                  output logic [1:0]   RegSrc,
                  output logic         RegWrite,
                  output logic [1:0]   ImmSrc,
                  output logic         ALUSrc,Mov,mov_reg,Shift,
                  output logic [2:0]   ALUControl,
                  output logic         MemWrite, MemtoReg,
                  output logic  [1:0]       PCSrc);

  logic [1:0] FlagW;
  logic       PCS, RegW, MemW,NoWrite,mov_inm,lsl,lsr;
  logic inmediato;
  assign inmediato = (Instr[20])? 1'b1:1'b0;
  logic cmd;
  assign cmd= (Instr[24:21]==4'b1101) ? 1'b1:1'b0;
  assign mov_reg =(~inmediato|Instr2==8'h00)&cmd;
  assign mov_inm =(inmediato|Instr2==8'h00)&cmd;
  assign Mov = mov_reg | mov_inm ;
  assign lsl = (~inmediato & Instr2[6:5]==2'b00)&cmd;
  assign lsr = (~inmediato & Instr2[6:5]==2'b01)&cmd;
  assign Shift = lsl | lsr ;
  
  decode dec(Instr[27:26], Instr[25:20], rd[15:12],
             FlagW, PCS, RegW, MemW,NoWrite,
             MemtoReg, ALUSrc, ImmSrc, RegSrc, ALUControl);
  condlogic cl(clk, reset,IRQ, Instr[31:28], ALUFlags,
               FlagW, PCS, RegW,MemW,NoWrite,
               PCSrc, RegWrite, MemWrite);
endmodule

module decode(input  logic [1:0] Op,
              input  logic [5:0] Funct,
              input  logic [3:0] Rd,
              output logic [1:0] FlagW,
              output logic       PCS, RegW, MemW,NoWrite,  //agregue mov 
              output logic       MemtoReg, ALUSrc,
              output logic [1:0] ImmSrc, RegSrc, 
              output logic [2:0]  ALUControl);

  logic [9:0] controls;
  logic       Branch, ALUOp;

  
	                        // Data processing immediate
 always_comb
  	casex(Op)
  	                        // Data processing immediate
  	  2'b00: if (Funct[5])  controls = 10'b0000101001; 
  	                        // Data processing register
  	         else           controls = 10'b0000001001; 
  	                        // LDR
  	  2'b01: if (Funct[0])  controls = 10'b0001111000; 
  	                        // STR
  	         else           controls = 10'b1001110100; 
  	                        // B
  	  2'b10:                controls = 10'b0110100010; 
  	                        // Unimplemented
  	  default:              controls = 10'bx;          
  	endcase

  assign {RegSrc, ImmSrc, ALUSrc, MemtoReg, 
          RegW, MemW, Branch, ALUOp} = controls; 
          
  // ALU Decoder             
  always_comb
    if(ALUOp)
    begin
    case(Funct[4:1])
    4'b0000:  begin  ALUControl=3'b110; NoWrite=1'b0;  end//and
    4'b0001:  begin  ALUControl=3'b100; NoWrite=1'b0; end//xor
    4'b0010:  begin  ALUControl=3'b001; NoWrite=1'b0; end//resta
    4'b0100:  begin  ALUControl=3'b000; NoWrite=1'b0; end //suma
    4'b1100:  begin  ALUControl=3'b101; NoWrite=1'b0; end//or
    4'b1010:  begin  ALUControl=3'b001; NoWrite=1'b1; end//resta para cmp  
    default:  begin  ALUControl=3'bx; NoWrite=1'b0; end//indefinido
    endcase
 
	// (C & V only updated for arith instructions)
      FlagW[1]      = Funct[0]; // FlagW[1] = S-bit
	// FlagW[0] = S-bit & (ADD | SUB)
      FlagW[0]      = Funct[0] & 
        (ALUControl == 3'b000 | ALUControl == 3'b001); 
    end else begin
      ALUControl = 3'b000; 
      FlagW      = 2'b00; // don't update Flags
    end
              
  // PC Logic
  assign PCS  = ((Rd == 4'b1111) & RegW) | Branch; 
endmodule

module condlogic(input  logic       clk, reset,IRQ,
                 input  logic [3:0] Cond,
                 input  logic [3:0] ALUFlags,
                 input  logic [1:0] FlagW,
                 input  logic       PCS, RegW, MemW,NoWrite,
                 input  logic [1:0] PCSrc,//se agrega bits para enable
                 output logic       RegWrite, MemWrite);
                 
  logic [1:0] FlagWrite;
  logic [3:0] Flags;
  logic       CondEx;
  logic       MemWrite1;

  flopenr #(2)flagreg1(clk, reset, FlagWrite[1], 
                       ALUFlags[3:2], Flags[3:2]);
  flopenr #(2)flagreg0(clk, reset, FlagWrite[0], 
                       ALUFlags[1:0], Flags[1:0]);

  condcheck cc(Cond, Flags, CondEx);
  assign FlagWrite = FlagW & {2{CondEx}};
  assign RegWrite  = RegW  & CondEx &~NoWrite;
  assign MemWrite1  = MemW  & CondEx;
  assign PCSrc     = PCS   & CondEx;
  
  assign MemWrite=(IRQ) ? 1'b1 : MemWrite1; //agregado para interrupciones
  assign PCSrc=(IRQ) ? 2'b10 : PCS   & CondEx ; //agregado para interrupciones
endmodule    

module condcheck(input  logic [3:0] Cond,
                 input  logic [3:0] Flags,
                 output logic       CondEx);
  
  logic neg, zero, carry, overflow, ge;
  
  assign {neg, zero, carry, overflow} = Flags;
  assign ge = (neg == overflow);
                  
  always_comb
    case(Cond)
      4'b0000: CondEx = zero;             // EQ
      4'b0001: CondEx = ~zero;            // NE
      4'b0010: CondEx = carry;            // CS
      4'b0011: CondEx = ~carry;           // CC
      4'b0100: CondEx = neg;              // MI
      4'b0101: CondEx = ~neg;             // PL
      4'b0110: CondEx = overflow;         // VS
      4'b0111: CondEx = ~overflow;        // VC
      4'b1000: CondEx = carry & ~zero;    // HI
      4'b1001: CondEx = ~(carry & ~zero); // LS
      4'b1010: CondEx = ge;               // GE
      4'b1011: CondEx = ~ge;              // LT
      4'b1100: CondEx = ~zero & ge;       // GT
      4'b1101: CondEx = ~(~zero & ge);    // LE
      4'b1110: CondEx = 1'b1;             // Always
      default: CondEx = 1'bx;             // undefined
    endcase
endmodule

module datapath(input  logic        clk, reset,
                input  logic [1:0]  RegSrc,
                input  logic        RegWrite,Shift,
                input  logic [1:0]  ImmSrc,
                input  logic        ALUSrc,
                input  logic [2:0]  ALUControl,
                input  logic        MemtoReg,
                input  logic        Mov,mov_reg,
                input  logic [1:0]  PCSrc,//se agrego mas bits para enable de pc 2
                output logic [3:0]  ALUFlags,
                output logic [31:0] PC,
                input  logic [31:0] Instr,
                output logic [31:0] ALUResult, WriteData, SrcA,SrcB,
                input  logic [31:0] ReadData);

  logic [31:0] PCNext, PCPlus4, PCPlus8,PCNext1;
  logic [31:0] ExtImm,Result,WD3,mov_out,inm_ext,dat_shift,dat;
  logic [3:0]  RA1, RA2,WA3;
  logic alu_flag_in=1'b0;
  // next PC logic
  mux2 #(32)  pcmux1(PCPlus4, Result, PCSrc, PCNext1);
  mux2PC #(32)  pcmux(PCPlus4, Result,PCNext1, PCSrc, PCNext);
  flopr #(32) pcreg(clk, reset, PCNext, PC);
  adder #(32) pcadd1(PC, 32'b100, PCPlus4);
  adder #(32) pcadd2(PCPlus4, 32'b100, PCPlus8);
  
  mux2 #(4) write_adress (Instr[15:12],Instr[19:16],Mov,WA3);  
  // register file logic
  mux2 #(4)   ra1mux(Instr[19:16], 4'b1111, RegSrc[0], RA1);
  mux2 #(4)   ra2mux(Instr[3:0], Instr[15:12], RegSrc[1], RA2);
  ext_imm8 mov_inmediato (Instr[7:0],inm_ext);
  
  shift_m shifter(WriteData,Instr[11:7],Instr[6:5],dat_shift);
  
  mux2 #(32) write_dato(Result,mov_out,Mov,dat);
  
  mux2 #(32) move_tipe (inm_ext,WriteData,mov_reg,mov_out);
  
  mux2 #(32) dato_guardado(dat,dat_shift,Shift,WD3); 
  
  regfile     rf(clk, RegWrite, RA1, RA2,
                 WA3, WD3, PCPlus8, 
                 SrcA, WriteData); 
               
                 
  mux2 #(32)  resmux(ALUResult, ReadData, MemtoReg, Result);
  extend      ext(Instr[23:0], ImmSrc, ExtImm);

  // ALU logic
  mux2 #(32)  srcbmux(WriteData, ExtImm, ALUSrc, SrcB);
  alu  #(32)  alu(.a(SrcA),.b(SrcB),.alu_flag_in(alu_flag_in),.opcode(ALUControl),
                    .flags(ALUFlags),.result(ALUResult)); 

                              
endmodule

module regfile(input  logic        clk, 
               input  logic        we3, 
               input  logic [3:0]  ra1, ra2, wa3, 
               input  logic [31:0] wd3, r15,
               output logic [31:0] rd1, rd2);

  logic [31:0] rf[14:0];
  
  initial 
  begin
  rf[0]=0;rf[1]=0;rf[2]=0;rf[3]=0;rf[4]=0;rf[5]=0;rf[6]=0;rf[7]=0;rf[8]=0;rf[9]=0;
  rf[10]=0;rf[11]=0;rf[12]=0;rf[13]=0;rf[14]=0; 
  end 


  always_ff @(negedge clk)     
    if (we3) begin rf[wa3] <= wd3; $display ("Dato de entrada",wd3);end
  always_ff@(posedge clk)
  
  $display ("Valor en registro",wa3,rf[wa3]);
  
  assign rd1 = (ra1 == 4'b1111) ? r15 : rf[ra1];
  assign rd2 = (ra2 == 4'b1111) ? r15 : rf[ra2];
endmodule

module extend(input  logic [23:0] Instr,
              input  logic [1:0]  ImmSrc,
              output logic [31:0] ExtImm);
 
  always_comb
    case(ImmSrc) 
               // 8-bit unsigned immediate
      2'b00:   ExtImm = {24'b0, Instr[7:0]};  
               // 12-bit unsigned immediate 
      2'b01:   ExtImm = {20'b0, Instr[11:0]}; 
               // 24-bit two's complement shifted branch 
      2'b10:   ExtImm = {{6{Instr[23]}}, Instr[23:0], 2'b00}; 
      default: ExtImm = 32'bx; // undefined
    endcase             
endmodule

module adder #(parameter WIDTH=8)
              (input  logic [WIDTH-1:0] a, b,
               output logic [WIDTH-1:0] y);
             
  assign y = a + b;
endmodule

module flopenr #(parameter WIDTH = 8)
                (input  logic             clk, reset, en,
                 input  logic [WIDTH-1:0] d, 
                 output logic [WIDTH-1:0] q=0);   //igual� a cero

  always_ff @(posedge clk, posedge reset)
    if (reset)   q <= 0;
    else if (en) q <= d;
endmodule

module flopr #(parameter WIDTH = 8)
              (input  logic             clk, reset,
               input  logic [WIDTH-1:0] d, 
               output logic [WIDTH-1:0] q);

  always_ff @(negedge clk, posedge reset) 
    if (reset) q <= 0;
    else       q <= d;
endmodule

module mux2 #(parameter WIDTH = 8)
             (input  logic [WIDTH-1:0] d0, d1, 
              input  logic             s, 
              output logic [WIDTH-1:0] y);

  assign y = s ? d1 : d0; 
endmodule

module mux2PC #(parameter WIDTH = 8)      //mux para pc, para agregar enable a pc
             (input  logic [WIDTH-1:0] a, b, c, 
              input  logic             s, 
              output logic [WIDTH-1:0] y);

  always_comb 
    begin
    case(s)
    0 : y = a ;
    1 : y = b ;
2 : y = c ;
    endcase
    end 
endmodule

module bit_adder(
   input  i_bit1,
  input  i_bit2,
  input  i_carry,
  output o_sum,
  output o_carry
    );
  wire   w_WIRE_1;
  wire   w_WIRE_2;
  wire   w_WIRE_3;
       
  assign w_WIRE_1 = i_bit1 ^ i_bit2;
  assign w_WIRE_2 = w_WIRE_1 & i_carry;
  assign w_WIRE_3 = i_bit1 & i_bit2;
 
  assign o_sum   = w_WIRE_1 ^ i_carry;
  assign o_carry = w_WIRE_2 | w_WIRE_3;
   
endmodule

module ext_imm8(
    input logic [7:0] imm_8,
    output logic [31:0]ext_imm
    );

assign ext_imm ={{24{imm_8[7]}},imm_8};
endmodule

module shift_m(
input logic [31:0] reg_in,
    input logic [4:0] shamt,
    input logic [1:0] type_shift,
    output logic [31:0] reg_shift
    );

logic [31:0] reg_shift_temp1,reg_shift_temp2;

always_comb
    begin
    case(type_shift)
    2'b00:  reg_shift = reg_in<< shamt;
    2'b01:  reg_shift = reg_in>>shamt;
    default reg_shift = reg_in;
    endcase             
end 
endmodule