`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 14.05.2019 17:12:06
// Design Name: 
// Module Name: Memory
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module Memory64(
    input wire [5:0] a,
    output wire [5:0] spo
    );
    
    parameter Datafile="Notas.mem";
    
    reg[5:0] ROM[0:63];
    
    assign spo=ROM[a];
    
    initial
    begin
    $readmemh (Datafile,ROM);
    end
endmodule