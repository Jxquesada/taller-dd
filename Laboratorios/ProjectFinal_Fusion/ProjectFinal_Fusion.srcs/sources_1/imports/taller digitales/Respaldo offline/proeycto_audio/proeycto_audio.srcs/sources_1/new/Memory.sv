`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 14.05.2019 17:12:06
// Design Name: 
// Module Name: Memory
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Memory1024(
    input wire [9:0] a,
    output wire [11:0] spo
    );
    
    parameter Datafile="Notas.mem";
    
    reg[11:0] ROM[0:1023];
    
    assign spo=ROM[a];
    
    initial
    begin
    $readmemh (Datafile,ROM);
    end
endmodule
