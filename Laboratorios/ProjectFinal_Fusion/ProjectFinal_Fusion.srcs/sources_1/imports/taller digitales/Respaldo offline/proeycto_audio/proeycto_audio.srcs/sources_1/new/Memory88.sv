`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 28.05.2019 11:17:49
// Design Name: 
// Module Name: Memory88
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Memory88(
    input wire [6:0] a,
    output wire [5:0] spo
    );
    
    parameter Datafile="Notas.mem";
    
    reg[5:0] ROM[0:128];
    
    assign spo=ROM[a];
    
    initial
    begin
    $readmemh (Datafile,ROM);
    end
endmodule
