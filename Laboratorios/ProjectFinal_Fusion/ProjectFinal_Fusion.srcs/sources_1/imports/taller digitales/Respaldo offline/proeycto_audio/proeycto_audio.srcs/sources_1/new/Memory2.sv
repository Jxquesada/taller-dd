`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 19.05.2019 14:28:25
// Design Name: 
// Module Name: Memory2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Memory_Parametric1(
    input  wire [5:0] a,
    output wire[11:0] spo
    );
    parameter Datafile="Notas.mem";
    reg[11:0] ROM[0:64];
    assign spo=ROM[a];
    
    initial
    begin
    $readmemh  (Datafile,ROM);
    end
endmodule
