`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 14.05.2019 16:15:43
// Design Name: 
// Module Name: Audio
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module Audio(
    input logic clk,
    input wire reset,
    input wire ps2d,ps2c,
   input wire [4:0] sw,//primeros 2 bits velocidad , 3 bits de musica
   output logic [4:1] led,
    output wire pwm,
    output logic aud_on
    );

   wire cout,cout1,cout0,clk_25;
   logic right ,left,start;
   logic [2:0] effect;
   wire [5:0] Song,Song1,Song2; 
    logic [9:0] sample=0,sample1=0,sample_ritmo=0,effect_sample;
    logic [5:0] addres=0;
    logic [6:0] addres1=0;
    wire [11:0] sen_frecuency,ritmo_frecuency;
    wire [11:0] dato_mix2,dato_mix1,dato_mix,data_ritmo, dato,effect_data;
    logic [11:0] dato_bass,dato_bass256;
    logic [31:0] velocidad, vel_count=0;
    wire [11:0] bass_frecuency;
    logic [11:0] sin_count=0;
    logic [12:0] bass_count=0;
    logic [11:0] ritmo_count=0;
    ;
    Memory1024#(.Datafile("sen.mem")) seno_melodic(sample,dato);
    Memory1024#(.Datafile("sen.mem")) seno_bass(sample1,dato_bass);
    Memory1024#(.Datafile("sen.mem")) seno_ritmo(sample_ritmo,data_ritmo);
     Memory1024#(.Datafile("sen.mem")) seno_effect(effect_sample,effect_data);
    Memory88 #(.Datafile("music2.mem")) music2(addres1,Song2);
    Memory64 #(.Datafile("music1.mem")) music1(addres,Song1);
   Memory64  #(.Datafile("music.mem")) music(addres,Song);
   Memory_Parametric1  #(.Datafile("Notas.mem")) notes2(Song1,bass_frecuency);
   Memory_Parametric1  #(.Datafile("Notas.mem")) notes(Song,sen_frecuency);
    Memory_Parametric1  #(.Datafile("Notas.mem")) notes3(Song2,ritmo_frecuency);
   keyboard1  boton( clk,reset,ps2c,ps2d,left,right,start,led[4]);
    SoundEffects sound(clk,effect,effect_sample);
    CarryLookAhead #(.nbits(12)) suma2channel(dato,dato_bass256,0,dato_mix,cout0);
    CarryLookAhead #(.nbits(12)) suma3channel(dato_mix,data_ritmo,cout0,dato_mix1,cout1); 
    CarryLookAhead #(.nbits(12)) suma4channel(dato_mix1,effect_data,cout1,dato_mix2,cout); 
    PWM2 mypwm(clk,reset,dato_mix2,pwm); 
always_comb
begin
effect[0]=left;
effect[1]=right;
effect[2]=start;
led[3]=start;
led[1]=left;
led[2]=right;
sample1[0]=0;
case(sw[1:0])
0:velocidad=32'd18000000;
1:velocidad=32'd20000000;
2:velocidad=32'd22000000;
3:velocidad=32'd25000000;
endcase
end
always@(posedge clk) 
 begin
     //sen_bass_velocity
     
     if(sw[4]==1) sample=10'd766;
     if(addres==63) 
        begin
        addres=0;
     
        end
      if(addres1==127) addres1=0;
      if(vel_count>=velocidad)
        begin
        addres=addres +1;     
        addres1=addres1+1;
        vel_count=0;
        end
        else vel_count = vel_count + 1;
    aud_on=1;
    //ritmo_frec
   
      if(sample_ritmo==1023) sample_ritmo=0; 
      if(sw[2]==1) sample_ritmo=10'd766;
        if(ritmo_frecuency==0)
        begin
        sample_ritmo=10'd766;
        ritmo_count=1;
        end
        if(ritmo_count==2986) ritmo_count=1;
        if(ritmo_count==ritmo_frecuency)
            begin
            sample_ritmo=sample_ritmo +1;
            ritmo_count=0;
            end
            else ritmo_count=ritmo_count + 1; 
    //sen_frec
    if(sample==1023) sample=0;
    if(sen_frecuency==0)
    begin
    sample=10'd766;
    sin_count=1;
    end
    if(sin_count==2986) sin_count=1;
    if(sin_count==sen_frecuency)
        begin
        sample=sample +1;
        sin_count=0;
        end
        else sin_count=sin_count + 1; 
    // bass_frecuency
      if(sample1 [9:1] ==512)  sample1[9:1]=0;
       if(sw[3]==1) sample1[9:1]=9'd383; //detiene el bajo hasta que el ciclo termine
         if(bass_frecuency==0)
        begin
        sample1 [9:1]=9'd383;
        bass_count=1;
        end
        if(bass_count==5972) bass_count=1;
        if(bass_count==bass_frecuency*2)
            begin
            sample1[9:1]=sample1[9:1] +1;
            bass_count=0;
            end
            else bass_count=bass_count + 1;
            dato_bass256=dato_bass/2;
//  if(sample1==1023) sample1=0;
//    if(bass_frecuency==0)
//    begin
//    sample1=10'd766;
//    bass_count=1;
//    end
//    if(bass_count==2986) bass_count=1;
//    if(bass_count==bass_frecuency)
//        begin
//        sample1=sample1 +1;
//        bass_count=0;
//        end
//        else bass_count=bass_count + 1; 
//         if(sw[2]==1) sample1=10'd766;
//ritmo
    end 
endmodule
