`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.05.2019 12:16:39
// Design Name: 
// Module Name: Ritmo
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SoundEffects(
input wire clk,
input wire [2:0] left_right,
 output wire [9:0] data
    );
    logic [9:0] frecuency_count=0;
    logic [9 :0] frecuency;
    logic  [9:0] data1=0;
    logic [25:0] velocidad=0;
   
     always@(posedge clk)
        begin
        if(velocidad>=10000000)      
        begin
         if(left_right==0) frecuency=10'h0;
        if(left_right==1) frecuency=10'heb;
        if(left_right==2) frecuency=10'hd1;
        if(left_right==4) frecuency=10'h69;
        velocidad=velocidad+1;
        end
        else
        begin
        if(left_right==0) frecuency=10'h0;
        if(left_right==1) frecuency=10'hd1;
        if(left_right==2) frecuency=10'hbb;
        if(left_right==4) frecuency=10'h69;
        velocidad=velocidad+1;
        end
        if(velocidad==20000000) velocidad=0;
        if(data1==1023) data1=0;
        if(frecuency==0) data1=10'd766;
        if(frecuency_count==frecuency)  
            begin
            frecuency_count=0;
            data1=data1+1;
            end
        else frecuency_count=frecuency_count + 1;
        end
        assign data=data1;
endmodule
