`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.06.2019 01:00:13
// Design Name: 
// Module Name: Score_hearts_Stop
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Score_hearts_Stop(
    input [9:0]Xpos,
    input [9:0]Ypos,
    input [31:0] OS,
    input [2:0] OL,
    output [5:0] BG2I
    );
    wire indexf,indexl;
    wire [9:0]roomaddresf;
    wire [5:0]roomaddres;
    assign roomaddres[2:0]=Xpos[2:0];
    assign roomaddres[5:3]=Ypos[2:0];
    
    
    assign roomaddresf [5:0]=roomaddres;
    assign roomaddresf [9:6]= (Xpos<584)?OS[31:28]:
                            (592>Xpos && Xpos>=584)?OS[27:24]:
                            (600>Xpos && Xpos>=592)?OS[23:20]:
                            (608>Xpos && Xpos>=600)?OS[19:16]:
                            (616>Xpos && Xpos>=608)?OS[15:12]:
                            (624>Xpos && Xpos>=616)?OS[11:8]:
                            (632>Xpos && Xpos>=624)?OS[7:4]:OS[3:0];
                            
    ROM640x1 Score(roomaddresf,indexf);
    ROM64x1 #(.Datafile("heart.mem")) Vidas(roomaddres,indexl);
    assign BG2I=//pausa
                (552>Xpos   &&  Xpos>=544  && Ypos<8  &&  OL[2]==1)?6'd13/*rojo*/:
                                //primer corazon
                (indexl==1  &&  560>Xpos  &&  Xpos>=552  &&  Ypos<8  &&  OL[1:0]>=3)?6'd13/*rojo*/:
                (indexl==1  &&  560>Xpos  &&  Xpos>=552  &&  Ypos<8  &&  OL[1:0]<3)?6'd12/*gris*/:
                                //segundo
                (indexl==1  &&  568>Xpos  &&  Xpos>=560  &&  Ypos<8  &&  OL[1:0]>=2)?6'd13:
                (indexl==1  &&  568>Xpos  &&  Xpos>=560  &&  Ypos<8  &&  OL[1:0]<2)?6'd12:
                                //tercero
                (indexl==1  &&  576>Xpos  &&  Xpos>=568  &&  Ypos<8  &&  OL[1:0]>=1)?5'd13:
                (indexl==1  &&  576>Xpos  &&  Xpos>=568  &&  Ypos<8  &&  OL[1:0]<1)?6'd12:
                //msb score
                (640>Xpos   &&  Xpos>=576 &&  Ypos<8     &&  indexf==1)?6'd63/*blanco*/:6'd0/*transparente*/;
endmodule
