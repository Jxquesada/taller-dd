`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 30.05.2019 14:01:30
// Design Name: 
// Module Name: Obj_Registers
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Obj_Registers(
    input wire clk,
    input wire Refresh,
    input wire WR_En,
    input  wire [31:0] Obj_data,
    output logic [19:0] Obj_player,
    output logic [20:0] Obj_1,
    output logic [20:0] Obj_2,
    output logic [20:0] Obj_3,
    output logic [31:0] Obj_score,
    output logic [2:0] Obj_live,
    output logic [3:0] Obj_addres
    );
    initial begin
    Obj_addres=4'b0;
    Obj_1[19:10]=10'd0;
    Obj_2[19:10]=10'd0;
    Obj_3[19:10]=10'd0;
    Obj_player[19:10]=10'd0;
    Obj_1[9:0]=10'd0;
    Obj_2[9:0]=10'd0;
    Obj_3[9:0]=10'd0;
    Obj_player[9:0]=10'd0;
    Obj_score=32'h76543210;
    Obj_live=3'h6;
    
    end
    
    always@(posedge clk)
    begin
    if (Refresh)
        begin
        if(Obj_addres==5)Obj_addres=15;
        else if(Obj_addres==15)Obj_addres=0;
        
        else
            Obj_addres=Obj_addres+1;
        end
    else Obj_addres=0;
        end
        
        
        
    always@(negedge clk)
    begin
    if(WR_En==1)begin
    case(Obj_addres)
    
0:  begin
    Obj_player<=Obj_data[19:0];
    end
    
1:  begin
    Obj_1[19:0]<=Obj_data[19:0];
    Obj_1[20]  <=Obj_data[31];
    end
    
2:  begin
    Obj_2[19:0]     <=Obj_data[19:0]; 
    Obj_2[20]       <=Obj_data[31];   
    end
    
3:  begin
    Obj_3[19:0]<=Obj_data[19:0]; 
    Obj_3[20]  <=Obj_data[31];   
    end
    
4:  begin
    Obj_score<=Obj_data;
    end
    
5:  begin
    Obj_live[1:0]<=Obj_data[1:0];
    Obj_live[2]  <=Obj_data[31];
    end
    endcase
    end
    end
        
endmodule
