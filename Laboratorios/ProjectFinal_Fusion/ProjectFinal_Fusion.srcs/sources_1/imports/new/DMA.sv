`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09.06.2019 18:53:14
// Design Name: 
// Module Name: DMA
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module DMA(
    input  wire IRQV,
    input  wire IRQT,
    input  wire IRQA,
    output wire IRAV,
    output wire IRAT,
    output wire IRAA,
    output wire IRQP
    );
    assign IRQP=(IRQV|IRQT|IRQA);
    assign IRAV=(IRQV)?1:0;
    assign IRAT=(IRQV==0)?1:0;
    assign IRAA=(IRQV==0&&IRAT==0)?1:0;
endmodule
