`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.06.2019 12:21:03
// Design Name: 
// Module Name: ROM256x4
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ROM256x4(
    input wire [7:0] IN_Addres,
    output wire [3:0] OUT_Data
    );
    
    parameter Datafile="Spritedata.txt";
    
    reg[3:0] ROM[0:255];
    
    assign OUT_Data=ROM[IN_Addres];
    
    initial
    begin
    $readmemh (Datafile,ROM);
    end
endmodule
