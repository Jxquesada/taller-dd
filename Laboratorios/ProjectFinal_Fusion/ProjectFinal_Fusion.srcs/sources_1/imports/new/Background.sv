`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.06.2019 22:19:10
// Design Name: 
// Module Name: Background
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Background(
    input wire [9:0] Xpos,
    input wire [9:0] Ypos,
    output wire [5:0] index
    );
    localparam CE0=0;//Color edificio 0 todos son indices
    localparam CE1=16;//Color edificio 1
    localparam CS0=24;//Color Suelo 0
    localparam CS1=23;//Color Suelo 1
    localparam CC=14; //Color del cielo
    
    
    wire indx;
    wire [7:0] romaddres;
    
    
    assign romaddres[3:0]=Xpos[3:0];
    assign romaddres[7:4]=Ypos[3:0];
    ROM256x1#(.Datafile("Ladrillos.mem")) ROMladrillos(romaddres,indx);
    
    assign index=(Xpos<128  &&  Ypos>=180  &&  Xpos<(480-Ypos)  &&  indx==0)?CE0:
                 (Xpos<128  &&  Ypos>=180  &&  Xpos<(480-Ypos)  &&  indx==1)?CE1:
                 (Ypos>340  &&  indx==0)?CS0:
                 (Ypos>340  &&  indx==1)?CS1:CC;
                    
   
endmodule
