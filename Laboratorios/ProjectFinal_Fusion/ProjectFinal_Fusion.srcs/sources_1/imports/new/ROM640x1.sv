`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.06.2019 11:52:18
// Design Name: 
// Module Name: ROM640x1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ROM640x1(
    input wire [9:0] IN_Addres,
    output wire  OUT_Data
    );
    
    parameter Datafile="font.mem";
    
    reg ROM[0:640];
    
    assign OUT_Data=ROM[IN_Addres];
    
    initial
    begin
    $readmemh (Datafile,ROM);
    end
endmodule


