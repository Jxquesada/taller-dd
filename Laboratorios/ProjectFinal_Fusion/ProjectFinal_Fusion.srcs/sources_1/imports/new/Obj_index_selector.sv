`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.06.2019 00:36:14
// Design Name: 
// Module Name: Obj_index_selector
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Obj_index_selector(
    input [9:0] Xpos,
    input [9:0] Ypos,
    input [19:0] OP,
    input [20:0] O1,
    input [20:0] O2,
    input [20:0] O3,
    input run,
    output [5:0] PIS//Palette index from Sprite
    );
    wire O1IR,O2IR,O3IR,PIR;
    
    assign O1IR=(O1[9:0]<=Xpos && Xpos<(O1[9:0]+64)  &&  O1[19:10]<=Ypos && Ypos <(O1[19:10]+64))?1:0;
    assign O2IR=(O2[9:0]<=Xpos && Xpos<(O2[9:0]+64)  &&  O2[19:10]<=Ypos && Ypos <(O2[19:10]+64))?1:0;
    assign O3IR=(O3[9:0]<=Xpos && Xpos<(O3[9:0]+64)  &&  O3[19:10]<=Ypos && Ypos <(O3[19:10]+64))?1:0;
    assign PIR=(OP[9:0] <=Xpos && Xpos<(OP[9:0]+128) &&  OP[19:10]<=Ypos && Ypos <(OP[19:10]+64))?1:0;
    
    wire [4:0]Xp;
    wire [9:0]preXp;
    wire [3:0]Yp,X1,Y1,X2,Y2,X3,Y3,prePIS;
    wire [9:0]preYp,preX1,preY1,preX2,preY2,preX3,preY3;
    
    wire [8:0]XYp;
    wire [7:0]XY1,XY2,XY3,XYR1,XYR2,XYR3;
    wire [3:0]PI,O1I,O2I,O3I,OS,OBI1,OBI2,OBI3;
    
    assign preXp=Xpos-OP[9:0];
    assign Xp=preXp[6:2];
    assign preYp=Ypos-OP[19:10];
    assign Yp=preYp[5:2];
    
    assign preX1=Xpos-O1[9:0];
    assign X1=preX1[5:2];
    assign preY1=Ypos-O1[19:10];
    assign Y1=preY1[5:2];
    
    assign preX2=Xpos-O2[9:0];
    assign X2=preX2[5:2];
    assign preY2=Ypos-O2[19:10];
    assign Y2=preY2[5:2];
    
    assign preX3=Xpos-O3[9:0];
    assign X3=preX3[5:2];
    assign preY3=Ypos-O3[19:10];
    assign Y3=preY3[5:2];
    
    assign XYp[4:0]=Xp;
    assign XYp[8:5]=Yp;
    
    assign XY1[3:0]=X1;
    assign XY1[7:4]=Y1;
    
    assign XY2[3:0]=X2;
    assign XY2[7:4]=Y2;
    
    assign XY3[3:0]=X3;
    assign XY3[7:4]=Y3;
    
    wire [7:0]XYOS;
    
                        logic [5:0]counter=0;
                    always @(posedge Ypos[9])
                    begin
                    if(run)counter=counter+1;
                    end
    
    
    
    assign XYOS=(O1[20]==0)?XY1:(O2[20]==0)?XY2:(O3[20]==1)?XY3:0;
    
    assign XYR1=(counter<20)?XY1:(counter<40)?XY2:XY3;
    assign XYR2=(counter<20)?XY3:(counter<40)?XY1:XY2;
    assign XYR3=(counter<20)?XY2:(counter<40)?XY3:XY1;
    
    ROM512x4 #(.Datafile("ObP.mem"))SpriteP(XYp,PI);
    ROM256x4 #(.Datafile("ObS.mem"))SpriteS(XYOS,OS);
    ROM256x4 #(.Datafile("Ob.mem"))SpriteO1(XYR1,O1I);
    ROM256x4 #(.Datafile("Ob1.mem"))SpriteO2(XYR2,O2I);
    ROM256x4 #(.Datafile("Ob2.mem"))SpriteO3(XYR2,O3I);
    
    assign OBI1=(O1[20]==0)?OS:(counter<20)?O1I:(counter<40)?O2I:O3I;
    assign OBI2=(O2[20]==0)?OS:(counter<20)?O3I:(counter<40)?O1I:O2I;
    assign OBI3=(O3[20]==0)?OS:(counter<20)?O2I:(counter<40)?O3I:O1I;
    
    
    assign prePIS=(|PI   &&  PIR )?PI:
                  (|OBI1  && O1IR)?OBI1:
                  (|OBI2 &&  O2IR)?OBI2:
                  (|OBI3 &&  O3IR)?OBI3:4'd0;
                 
                
       
    assign PIS[3:0]=prePIS;
    assign PIS[5:4]=
                    (|OBI1  && O1IR)?2'b1:
                    (|OBI2 &&  O2IR)?2'd2:
                    (|OBI3 &&  O3IR)?2'd3:2'b0;
                    
                    

endmodule
