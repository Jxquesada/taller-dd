`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.04.2019 12:18:34
// Design Name: 
// Module Name: VGA_Controller
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module VGA_Controller(
    input   wire    IN_Pixel_CLK,
    output  wire    OUT_Hsync,
    output  wire    OUT_Vsync,
    output  wire    [9:0] OUT_Xpos,
    output  wire    [9:0] OUT_Ypos,
    output  wire    OUT_VideoOn
    );
    
    // Definicion de parametros Resolucion
    localparam  HD = 640;    //area de visualizacion horizontal
    localparam  HF = 16;     //Front porch Horizontal
    localparam  HB = 48;     //back porch horizontal
    localparam  HR = 96;     //retrace horizontal
    localparam  VD = 480;    //area de visualizacion vertical
    localparam  VF = 10;     //Front porch vertical
    localparam  VB = 33;     //back porch vertical
    localparam  VR = 2;      //retrace vertical
    
    reg V_on,Hsync,Vsync;//
    
    //contadores para ancho y alto de resolucion
    reg [9:0] INTERNAL_Hcounter;
    reg [9:0] INTERNAL_Vcounter;//cuentan de 0 hasta (2^nbits)-1  (-1+2^10=1023)
    
    //conectando los registros a las salidas
    //Logica de Hsync, Vsync  y Video_ON
    assign OUT_VideoOn  = (INTERNAL_Hcounter<HD && INTERNAL_Vcounter<VD)? 1'b1:1'b0;
    assign OUT_Hsync    = (INTERNAL_Hcounter>=(HD+HF) && INTERNAL_Hcounter<(HD+HF+HR))?  1'b1:1'b0;
    assign OUT_Vsync    = (INTERNAL_Vcounter>=(VD+VF) && INTERNAL_Vcounter<(VD+VF+VR))?  1'b1:1'b0;
    assign OUT_Xpos     = INTERNAL_Hcounter;
    assign OUT_Ypos     = INTERNAL_Vcounter;
    
    //iniciando los contadores en el vsync alto
 initial
    begin
        INTERNAL_Hcounter=0;
        INTERNAL_Vcounter=0;
    end
    
    // Logica de los contadores
    always @(posedge IN_Pixel_CLK)
        begin
        if (INTERNAL_Hcounter==(HD+HF+HB+HR-1))
            begin
            INTERNAL_Hcounter <= 10'b0;
            end
        if (INTERNAL_Hcounter==(HD+HF-1))
            begin
            if  (INTERNAL_Vcounter<(VD+VF+VB+VR-1))
                begin
                INTERNAL_Vcounter <=INTERNAL_Vcounter+1;
                end
            else
                begin
                INTERNAL_Vcounter <= 10'b0;
                end
            end
            
        if (INTERNAL_Hcounter<(HD+HF+HB+HR-1))
            begin
            INTERNAL_Hcounter <= INTERNAL_Hcounter+1;
            end
        end
        
endmodule
