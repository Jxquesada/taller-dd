`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.06.2019 18:27:28
// Design Name: 
// Module Name: ROMtrace
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ROMtrace(
    input wire [5:0] IN_Addres,
    output wire  [9:0]OUT_Data
    );
    
    parameter Datafile="Spritedata.txt";
    
    reg [9:0]ROM[0:49];
    
    assign OUT_Data=ROM[IN_Addres];
    
    initial
    begin
    $readmemh (Datafile,ROM);
    end
endmodule

