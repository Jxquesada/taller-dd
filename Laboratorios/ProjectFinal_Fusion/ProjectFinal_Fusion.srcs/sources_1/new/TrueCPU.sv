`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.06.2019 12:42:50
// Design Name: 
// Module Name: TrueCPU
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TrueCPU(
    input clk,
    input vsync,
    input [3:0]DI_KBRD,
    input [3:0]Obaddres,
    output[31:0]OBjData
    
    
    );
    parameter Datafile="Trayectoriax.mem";
    parameter Datafile2="Trayectoriay.mem";
    logic [19:0]player;
    logic [20:0]fly1;
    wire [20:0]fly2,fly3;
    logic [31:0]score;
    logic [2:0]live;
    
    assign OBjData[1:0]=(Obaddres==0)?player[1:0]:(Obaddres==1)?fly1[1:0]:(Obaddres==2)?fly2[1:0]:(Obaddres==3)?fly3[1:0]:(Obaddres==4)?score[1:0]:live[1:0];
    assign OBjData[19:2]=(Obaddres==0)?player[19:2]:(Obaddres==1)?fly1[19:2]:(Obaddres==2)?fly2[19:2]:(Obaddres==3)?fly3[19:2]:(Obaddres==4)?score[19:2]:0;
    assign OBjData[30:20]=(Obaddres==0)?0:(Obaddres==1)?0:(Obaddres==2)?0:(Obaddres==3)?0:(Obaddres==4)?score[30:20]:0;
    assign OBjData[31]=(Obaddres==0)?0:(Obaddres==1)?fly1[20]:(Obaddres==2)?fly2[20]:(Obaddres==3)?fly3[20]:(Obaddres==4)?score[31]:live[2];
    
    wire [9:0]nextx,nexty;

    
    assign fly2[9:0]=10'd64; 
    assign fly2[19:9]=10'd400;
    assign fly2[20]=10'd0;
    assign fly3[9:0]=10'd0; 
    assign fly3[19:9]=10'd400;
    assign fly3[20]=10'd0;
    initial begin
    player[9:0]=10'd128;
    player[19:9]=10'd400;
    fly1[9:0]=10'd64; 
    fly1[19:9]=10'd116;
    fly1[20]=0;

    score=0; 
    live[2:0]=5;
    end
    
    logic state=0;
    logic hold=0;
    logic [1:0]playerstate=0;
    logic [5:0]counterfly=0;
    
    logic timer=0;

    
    ROMtrace #(.Datafile("Trayectoriax.mem"))ROMtracex(counterfly,nextx);
    ROMtrace #(.Datafile("Trayectoriay.mem"))ROMtracey(counterfly,nexty);
    
    always @(posedge clk)
    begin
    case(state)
    
    
0:  begin
    if(DI_KBRD[2]&&hold==0)begin
    live[2]=0;
    hold=1;
    state=1;
    fly1[20]=1;
    end
    end
    
1:  begin
    

    if(~|DI_KBRD&&hold==1)hold=0;
    
    if(live[1:0]==0)begin
    live<=3'd5;
    counterfly=0;
    fly1[9:0]= 10'd64;
    fly1[19:0]=10'd116;
    state=0;
    score=0;
    fly1[20]=0;
    end
    
    else 
    begin
        
        if (fly1[9:0]==160&&fly1[19:10]==336&&playerstate!=0 || fly1[9:0]==320&&fly1[19:10]==336&&playerstate!=1 ||fly1[9:0]==480&&fly1[19:10]==336&&playerstate!=2)
        begin
        counterfly<=0;
        fly1[9:0]<= 10'd64;  
        fly1[19:0]<=10'd116; 
        live[1:0]<=live[1:0]-1;
        end
    
        else if (fly1[9:0]==570&&fly1[19:10]==212)///gana
        begin
            counterfly=0;
            if (score[3:0]==9)begin 
            score[7:4]<=score[7:4]+1;
            score[3:0]<=0;
            fly1[9:0]<=64; 
            fly1[19:9]<=116;end
            else 
            begin 
            score[3:0]<=score[3:0]+1;
            end
        end
    
        else 
        begin
            
            if (timer==0&&vsync==1)begin
            counterfly<=counterfly+1;
            timer=1;
            end
            else if (timer==1&&vsync==0)begin
            timer=0;
            end
            fly1[9:0]<=nextx;
            fly1[19:0]<=nexty;
            if (DI_KBRD[0]&&hold==0)begin
                if(playerstate!=2)begin
                    player[9:0]=player[9:0]+128;
                    playerstate=playerstate+1;
                    end
                hold=1;
            end
            
            else if(DI_KBRD[1]&&hold==0)begin
                if(playerstate!=0)begin
                    player[9:0]=player[9:0]-128;
                    playerstate=playerstate-1;
                end
                hold=1;
            end
            
            else if(DI_KBRD[2]==1&&hold==0)begin
                live[2]=1;
                end
        
       
    
    end
    
    end
    end
    endcase
    end
endmodule
