`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.06.2019 12:20:21
// Design Name: 
// Module Name: GG2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module GG2(
    input wire clk,
    input wire audoff,
     input wire ps2d,ps2c,
    output wire Hsync,
    output wire Vsync,
    output wire [11:0]RGB,
    output wire pwm,
    output logic aud_on
    );
    
    
    wire IRQV,IRAV,PPU_MEMRD,PPU_MEMWR,G_MEMWR;
    wire [3:0] DIR_CPU,DIR_PPU,DIR_AUD,DIR_MEM,DIR_KBRD;
    wire [31:0] DI_CPU,DI_PPU,DI_MEM,DO_MEM;
    wire [3:0] DI_KBRD;
    
    TrueCPU CPU(
    clk,
    Vsync,
    DI_KBRD,
    DIR_PPU,
    DO_MEM

    );
    //////////////////////////////PICTURE PROCESING UNIT///////////////////////////////////////
    PPU PPU0(
    clk,
    DO_MEM,
    IRQV,
    RGB,
    Vsync,
    Hsync,
    DIR_PPU,
    IRQV,
    DI_PPU,
    PPU_MEMRD,
    PPU_MEMWR
    );
    
  //////////////////DIRECT MEMORY ACCESS///////////////////////////////////////////  
    /*
    DMA DMA0(
    IRQV,
    IRQT,
    IRQA,
    IRAV,
    IRAT,
    IRAA,
    IRQP
    );*/
   //////////////////////////////////AUDIO/TECLADO/////////////////////////////////// 
    Audio Audio0(
    clk,
    audoff,
    ps2d,ps2c,
   5'd26,
     DI_KBRD[3:0],
     pwm,
    aud_on
    );
   
    //////////////////////////////////MEMORIA///////////////////////////////////
    //asignacion de buses de memoria
  /*   assign DIR_MEM=(DIR_PPU=='bz&&DIR_KBRD=='bz&&DIR_AUD=='bz)?DIR_CPU:
    (DIR_CPU=='bz&&DIR_KBRD=='bz&&DIR_AUD=='bz)?DIR_PPU:
     (DIR_CPU=='bz&&DIR_PPU=='bz&&DIR_AUD=='bz)?DIR_KBRD:DIR_AUD;*/
    
 /*   assign DI_MEM=(DI_PPU=='bz&&DI_KBRD=='bz)?DI_CPU:
    (DI_CPU=='bz&&DI_KBRD=='bz)?DI_PPU:DI_KBRD;
    //asignacion de RD WR en la memoria
    assign G_MEMWR=(PPU_MEMWR&CPU_MEMWR&KBRD_MEMWR);
    RAM RAM0(
    clk,
    0,
    G_MEMWR,
    DIR_MEM,
    DI_MEM,
    DO_MEM
    );
    */
endmodule
