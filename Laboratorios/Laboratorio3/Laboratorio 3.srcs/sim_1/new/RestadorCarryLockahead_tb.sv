`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 14.03.2019 14:06:15
// Design Name: 
// Module Name: RestadorCarryLockahead_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module RestadorCarryLockahead_tb(
    );
parameter nbits = 4;
logic [nbits :1] a, b;
logic cin;
logic [nbits :1] s;
logic cout;
RestadorCarryLockahead #(.nbits(4))RestCarrylooktest(.a(a),.b(b),.cin(cin),.cout(cout),.s(s));
initial
begin
a=4'b0111;
b=4'b0101;
cin=1'b0;
end

endmodule

