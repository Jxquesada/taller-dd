`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Johan Chaves Zamora
// 
// 
// Create Date: 12.03.2019 13:47:01
// Design Name: 
// Module Name: Carrylookahead_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Carrylookahead_tb(
    );
parameter nbits = 4;
logic [nbits :1] a, b;
logic cin;
logic [nbits :1] s;
logic cout;
CarryLookAhead #(.nbits(4))Carrylooktest(.a(a),.b(b),.cin(cin),.cout(cout),.s(s));
initial
begin
a=4'b1111;
b=4'b0101;
cin=1'b1;
end

endmodule
