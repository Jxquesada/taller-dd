`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.03.2019 14:47:40
// Design Name: 
// Module Name: Restador_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Restador_tb(
    );
parameter nbits = 4;
logic [nbits :1] a, b;
logic cin;
logic [nbits :1] s;
logic cout;
RestadorCarryLockahead #(.nbits(4)) Restadorttest(.cin(cin),.a(a),.b(b),.cout(cout),.s(s));
initial
begin
a=4'b1111;
b=4'b1000;
cin=1'b0;
end

endmodule
