`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.03.2019 15:24:18
// Design Name: 
// Module Name: ALU_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALU_TB(
    );
parameter nbits = 4;
logic [3:0] seleccion;
logic [nbits-1 :0] a, b,s;

logic ALUFLAG1;
wire [6 :0] y;
logic N,Z,C,V;
Main_ALU#(.nbits(4))ALUtest(.seleccion(seleccion),.a(a),.b(b),.ALUFLAG1(ALUFLAG1),.s(s),.y(y),.N(N),.Z(Z),.C(C),.V(V));
initial
begin
a=4'b0111;
b=4'b0001;
ALUFLAG1=1'b0;
seleccion=4'b0010;
end
endmodule
