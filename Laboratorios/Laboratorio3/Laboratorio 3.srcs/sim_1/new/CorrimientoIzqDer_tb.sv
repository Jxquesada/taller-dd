`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 17.03.2019 16:51:41
// Design Name: 
// Module Name: CorrimientoIzqDer_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module CorrimientoIzqDe_tb(
    );
parameter nbits = 4;
logic [nbits :1] a,b;
logic ALUFLAG1;
logic [nbits :1] s,s1;
logic c,c1;
int b1;
Corrimiento #(.nbits(4))corrimiento_TB2(.a(a),.b(b),.ALUFLAG1(ALUFLAG1),.c(c),.c1(c1),.s(s),.s1(s1),.b1(b1));
initial
begin
a=4'b1001;
b=4'b0010;
ALUFLAG1=1'b1;
end
endmodule
