`timescale 1ns / 1ns
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.03.2019 15:21:08
// Design Name: 
// Module Name: OverflowFlag_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module OverflowFlag_TB(
   );
    logic AMSB;
    logic BMSB;
    logic SMSB;
    logic SignMode;
    wire FlagOverflow;
    
    OverflowFlag OverflowFlagTest(
    .AMSB(AMSB),
    .BMSB(BMSB),
    .SMSB(SMSB),
    .SignMode(SignMode),
    .FlagOverflow (FlagOverflow)
    );
    initial
        begin
        AMSB    <=1'b0;
        BMSB    <=1'b0;
        SMSB    <=1'b0;
        SignMode<=1'b0;
        #100
        AMSB    <=1'b1;
        BMSB    <=1'b0;
        SMSB    <=1'b0;
        SignMode<=1'b0;
        #100
        AMSB    <=1'b1;
        BMSB    <=1'b1;
        SMSB    <=1'b0;
        SignMode<=1'b0;
        #100
        AMSB    <=1'b0;
        BMSB    <=1'b0;
        SMSB    <=1'b1;
        SignMode<=1'b0;
        #100
        AMSB    <=1'b1;
        BMSB    <=1'b0;
        SMSB    <=1'b1;
        SignMode<=1'b0;
        #100
        AMSB    <=1'b1;
        BMSB    <=1'b1;
        SMSB    <=1'b1;
        SignMode<=1'b0;
        #100
        AMSB    <=1'b0;
        BMSB    <=1'b0;
        SMSB    <=1'b0;
        SignMode<=1'b1;
        #100         
        AMSB    <=1'b1;
        BMSB    <=1'b0;
        SMSB    <=1'b0;
        SignMode<=1'b1;
        #100         
        AMSB    <=1'b1;
        BMSB    <=1'b1;
        SMSB    <=1'b0;
        SignMode<=1'b1;
        #100         
        AMSB    <=1'b0;
        BMSB    <=1'b0;
        SMSB    <=1'b1;
        SignMode<=1'b1;
        #100         
        AMSB    <=1'b1;
        BMSB    <=1'b0;
        SMSB    <=1'b1;
        SignMode<=1'b1;
        #100         
        AMSB    <=1'b1;
        BMSB    <=1'b1;
        SMSB    <=1'b1;
        SignMode<=1'b1;
        #100;
        end             
endmodule
