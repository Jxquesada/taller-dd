`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 17.03.2019 15:37:16
// Design Name: 
// Module Name: DecoBiDec
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module Corrimiento #(parameter nbits=4)(
input logic [nbits-1 :0] a,
input logic [nbits-1 :0] b,
input logic ALUFLAG1,
output logic c,
output logic c1,
output logic [nbits-1:0] s,
output logic [nbits-1:0] s1
    );
 logic  [nbits-1:0] G,P,P1;
 integer b1;
 logic m,n;
 always_comb
 begin
   b1=b;
   if(ALUFLAG1==0)
        begin
        G = a >> b1;
        P = a << b1;
        end
     else
       begin 
           G=a;
           for (int i=0; i <= nbits-1;i++)
            begin
            P[nbits-1-i]=a[i];
            end
           if(P[nbits-1]==1)
                begin
                P = $signed(P) >>> b1;
                end
            else
                begin
                P[nbits-1]=1'b1;
                P = $signed(P) >>> b1;
                 if (b1<nbits-1)
                    begin
                    P[nbits-1-b]=0;
                    end
            end
                   
           if(a[nbits-1]==1)
             begin
                G = $signed(G) >>> b1;
             end
           else
             begin
                 G[nbits-1] = 1'b1;
                 G = $signed(G) >>> b1;
                 if (b1<nbits-1)
                    begin
                    G[nbits-1-b]=0;
                    end
             end

            for (int i=0; i <= nbits-1;i++)
            begin
            P1[nbits-1-i]=P[i];
            end
            P=P1;
         end
 m = a[nbits-b1];
 n = a[b1-1];
 end
 assign c=m;
 assign c1=n;
 assign s=G;
 assign s1=P;
endmodule
