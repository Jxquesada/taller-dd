`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.03.2019 15:15:47
// Design Name: 
// Module Name: NegativeFlag
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module NegativeFlag(
    output logic FlagNegative,
    
    input wire AMSB,
    input wire BMSB,
    input wire SMSB,
    input wire Sign
    );
    assign FlagNegative = Sign&~(~(AMSB&BMSB)&~(SMSB&(AMSB|BMSB)));
endmodule
