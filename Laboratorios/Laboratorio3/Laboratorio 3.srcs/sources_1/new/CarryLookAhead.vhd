----------------------------------------------------------------------------------
-- Johan Chaves Zamora
--  
-- 
-- Create Date: 12.03.2019 13:30:58
-- Design Name: 
-- Module Name: CarryLookAhead - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;


ENTITY CarryLookAhead IS

<<<<<<< Updated upstream
Generic(nbits:integer:=23);--n bits es eln�mero de bits del sumador
=======
Generic(nbits:integer:=32);--n bits es eln�mero de bits del sumador
>>>>>>> Stashed changes
PORT(a,b:in std_logic_vector(nbits downto 1);
cin:in std_logic;
s:out std_logic_vector(nbits downto 1);
cout:out std_logic);

end CarryLookAhead;

architecture Behavioral of CarryLookAhead is
constant tp:time:= 10 ns;--retardo de propagaci�n 
signal C:std_logic_vector(nbits downto 0);
signal G,P:std_logic_vector(nbits downto 1);
component FULLADDER
PORT(a,b:in std_logic;
cin:in std_logic;
s:out std_logic;
cout:out std_logic);
END component;
Begin
C(0) <= cin;
 G <= a and b after tp;
  P <= a xor b after tp; 
  -- Generaci�n de acarreos anticipados 
  acarreo: for j in 1 to nbits generate
   C(j) <= G(j) or (P(j) and C(j-1)); -- Expresi�n del acarreo anticipado 
   end generate acarreo; 
   cout <= C(nbits) after 2*tp;
    sum_ant: for i in 1 to nbits generate
     elemento_sumador:FULLADDER port map( a => a(i),
      b => b(i),
      cin => C(i-1),
      s => s(i), 
      cout => open);
      End generate sum_ant;
end Behavioral;
