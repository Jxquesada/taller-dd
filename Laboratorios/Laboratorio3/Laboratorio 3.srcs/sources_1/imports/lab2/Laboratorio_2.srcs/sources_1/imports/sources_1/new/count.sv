`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Tecnologico de Costa Rica
// Taller de Dise�o digital 
// Johan Chaves Zamora 2016062523
// Create Date: 19.02.2019 19:58:21
// Design Name: 
// Module Name: Count
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module Count(
    input wire clock,
    input wire reset,
    output logic [3:0] count
    );
    initial 
        begin
        count=4'b0000;
        end
     always @ (posedge clock)
        begin
        if (reset)
            begin
            count=4'b0000;
            end
        else
            begin
            count<=count+1;
            end
        end
endmodule
