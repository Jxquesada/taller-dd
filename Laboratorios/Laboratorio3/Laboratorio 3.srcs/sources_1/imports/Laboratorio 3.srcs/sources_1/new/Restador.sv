`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12.03.2019 13:22:00
// Design Name: 
// Module Name: Restador
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module RestadorCarryLockahead #(parameter nbits=4)(input logic cin,
input logic [nbits-1:0] a,
input logic [nbits-1:0] b,
output logic cout,
output logic [nbits-1:0] s,
output logic BcomplementMSB
    );
  logic [nbits-1:0] b1,b1plus;
  Inversor #(nbits)Inversor1(b,b1);
  CarryLookAhead #(nbits)Carrylooktest1(a,b1plus,cin,s,cout);
   
  assign b1plus=b1+1;
  assign BcomplementMSB=b1plus[nbits-1];
endmodule
