// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
// Date        : Sun Mar 24 19:08:26 2019
// Host        : Johan running 64-bit major release  (build 9200)
// Command     : write_verilog -mode funcsim -nolib -force -file {C:/Users/JohanAndres/Desktop/taller
//               digitales/proyecto-taller-digitales/Laboratorios/Laboratorio3/Laboratorio
//               3.sim/sim_1/synth/func/xsim/ALU_TB_func_synth.v}
// Design      : Main_ALU
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* nbits = "4" *) 
(* NotValidForBitStream *)
module Main_ALU
   (seleccion,
    a,
    b,
    ALUFLAG1,
    y,
    N,
    Z,
    C,
    V,
    dp,
    an,
    Sign);
  input [3:0]seleccion;
  input [3:0]a;
  input [3:0]b;
  input ALUFLAG1;
  output [6:0]y;
  output N;
  output Z;
  output C;
  output V;
  output dp;
  output [7:0]an;
  input Sign;

  wire ALUFLAG1;
  wire ALUFLAG1_IBUF;
  wire C;
  wire C_OBUF;
  wire C_OBUF_inst_i_11_n_0;
  wire C_OBUF_inst_i_12_n_0;
  wire C_OBUF_inst_i_13_n_0;
  wire C_OBUF_inst_i_2_n_0;
  wire C_OBUF_inst_i_3_n_0;
  wire C_OBUF_inst_i_8_n_0;
  wire [2:2]G02_in;
  wire N;
  wire N_OBUF;
  wire N_OBUF_inst_i_2_n_0;
  wire N_OBUF_inst_i_4_n_0;
  wire N_OBUF_inst_i_6_n_0;
  wire N_OBUF_inst_i_7_n_0;
  wire N_OBUF_inst_i_8_n_0;
  wire N_OBUF_inst_i_9_n_0;
  wire [3:1]P02_in;
  wire \Resta/Carrylooktest1/cin2_out ;
  wire \Resta/Carrylooktest1/p_0_in3_in ;
  wire [3:0]\Selector1/s__45 ;
  wire Sign;
  wire Sign_IBUF;
  wire \Suma/cin1_in ;
  wire \Suma/cin2_out ;
  wire \Suma/sum_ant[4].elemento_sumador/p__0 ;
  wire V;
  wire V_OBUF;
  wire Z;
  wire Z_OBUF;
  wire [3:0]a;
  wire [3:0]a_IBUF;
  wire [7:0]an;
  wire [1:0]an_OBUF;
  wire awire;
  wire [3:0]b;
  wire [3:0]b_IBUF;
  wire c1;
  wire c5;
  wire c6;
  wire data2;
  wire data4;
  wire data5;
  wire [3:0]data8;
  wire [3:1]data9;
  wire dp;
  wire dp_OBUF;
  wire [2:0]p_0_in;
  wire [3:3]s2;
  wire [3:0]seleccion;
  wire [3:0]seleccion_IBUF;
  wire signwire;
  wire [6:0]y;
  wire [6:0]y_OBUF;
  wire \y_OBUF[6]_inst_i_10_n_0 ;
  wire \y_OBUF[6]_inst_i_11_n_0 ;
  wire \y_OBUF[6]_inst_i_12_n_0 ;
  wire \y_OBUF[6]_inst_i_13_n_0 ;
  wire \y_OBUF[6]_inst_i_14_n_0 ;
  wire \y_OBUF[6]_inst_i_15_n_0 ;
  wire \y_OBUF[6]_inst_i_16_n_0 ;
  wire \y_OBUF[6]_inst_i_18_n_0 ;
  wire \y_OBUF[6]_inst_i_19_n_0 ;
  wire \y_OBUF[6]_inst_i_20_n_0 ;
  wire \y_OBUF[6]_inst_i_24_n_0 ;
  wire \y_OBUF[6]_inst_i_25_n_0 ;
  wire \y_OBUF[6]_inst_i_28_n_0 ;
  wire \y_OBUF[6]_inst_i_29_n_0 ;
  wire \y_OBUF[6]_inst_i_30_n_0 ;
  wire \y_OBUF[6]_inst_i_31_n_0 ;
  wire \y_OBUF[6]_inst_i_32_n_0 ;
  wire \y_OBUF[6]_inst_i_36_n_0 ;
  wire \y_OBUF[6]_inst_i_37_n_0 ;
  wire \y_OBUF[6]_inst_i_38_n_0 ;
  wire \y_OBUF[6]_inst_i_39_n_0 ;
  wire \y_OBUF[6]_inst_i_41_n_0 ;
  wire \y_OBUF[6]_inst_i_43_n_0 ;
  wire \y_OBUF[6]_inst_i_44_n_0 ;
  wire \y_OBUF[6]_inst_i_45_n_0 ;
  wire \y_OBUF[6]_inst_i_46_n_0 ;
  wire \y_OBUF[6]_inst_i_48_n_0 ;
  wire \y_OBUF[6]_inst_i_52_n_0 ;
  wire \y_OBUF[6]_inst_i_53_n_0 ;
  wire \y_OBUF[6]_inst_i_54_n_0 ;
  wire \y_OBUF[6]_inst_i_55_n_0 ;
  wire \y_OBUF[6]_inst_i_56_n_0 ;
  wire \y_OBUF[6]_inst_i_57_n_0 ;
  wire \y_OBUF[6]_inst_i_6_n_0 ;
  wire \y_OBUF[6]_inst_i_7_n_0 ;
  wire \y_OBUF[6]_inst_i_8_n_0 ;
  wire \y_OBUF[6]_inst_i_9_n_0 ;

  IBUF ALUFLAG1_IBUF_inst
       (.I(ALUFLAG1),
        .O(ALUFLAG1_IBUF));
  OBUF C_OBUF_inst
       (.I(C_OBUF),
        .O(C));
  MUXF7 C_OBUF_inst_i_1
       (.I0(C_OBUF_inst_i_2_n_0),
        .I1(C_OBUF_inst_i_3_n_0),
        .O(C_OBUF),
        .S(seleccion_IBUF[0]));
  LUT5 #(
    .INIT(32'hFBEC20C8)) 
    C_OBUF_inst_i_10
       (.I0(ALUFLAG1_IBUF),
        .I1(b_IBUF[0]),
        .I2(a_IBUF[0]),
        .I3(b_IBUF[1]),
        .I4(a_IBUF[1]),
        .O(\Resta/Carrylooktest1/cin2_out ));
  LUT2 #(
    .INIT(4'h1)) 
    C_OBUF_inst_i_11
       (.I0(b_IBUF[1]),
        .I1(b_IBUF[0]),
        .O(C_OBUF_inst_i_11_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    C_OBUF_inst_i_12
       (.I0(a_IBUF[1]),
        .I1(a_IBUF[0]),
        .I2(a_IBUF[3]),
        .I3(a_IBUF[2]),
        .O(C_OBUF_inst_i_12_n_0));
  LUT4 #(
    .INIT(16'h8000)) 
    C_OBUF_inst_i_13
       (.I0(a_IBUF[1]),
        .I1(a_IBUF[0]),
        .I2(a_IBUF[3]),
        .I3(a_IBUF[2]),
        .O(C_OBUF_inst_i_13_n_0));
  LUT6 #(
    .INIT(64'h000FC0A00000C0A0)) 
    C_OBUF_inst_i_2
       (.I0(c1),
        .I1(data5),
        .I2(seleccion_IBUF[1]),
        .I3(seleccion_IBUF[3]),
        .I4(seleccion_IBUF[2]),
        .I5(c5),
        .O(C_OBUF_inst_i_2_n_0));
  LUT6 #(
    .INIT(64'h0020333000200030)) 
    C_OBUF_inst_i_3
       (.I0(data4),
        .I1(seleccion_IBUF[2]),
        .I2(C_OBUF_inst_i_8_n_0),
        .I3(seleccion_IBUF[1]),
        .I4(seleccion_IBUF[3]),
        .I5(c6),
        .O(C_OBUF_inst_i_3_n_0));
  LUT6 #(
    .INIT(64'hFBB2EFCF2000CB82)) 
    C_OBUF_inst_i_4
       (.I0(\Resta/Carrylooktest1/cin2_out ),
        .I1(b_IBUF[2]),
        .I2(C_OBUF_inst_i_11_n_0),
        .I3(a_IBUF[2]),
        .I4(b_IBUF[3]),
        .I5(a_IBUF[3]),
        .O(c1));
  LUT6 #(
    .INIT(64'hAFFCA0FCAF0CA00C)) 
    C_OBUF_inst_i_5
       (.I0(a_IBUF[1]),
        .I1(a_IBUF[0]),
        .I2(b_IBUF[1]),
        .I3(b_IBUF[0]),
        .I4(a_IBUF[3]),
        .I5(a_IBUF[2]),
        .O(data5));
  LUT6 #(
    .INIT(64'hFFFEFFFFFFFE0000)) 
    C_OBUF_inst_i_6
       (.I0(b_IBUF[1]),
        .I1(b_IBUF[0]),
        .I2(b_IBUF[2]),
        .I3(b_IBUF[3]),
        .I4(ALUFLAG1_IBUF),
        .I5(C_OBUF_inst_i_12_n_0),
        .O(c5));
  LUT6 #(
    .INIT(64'hFCAF0CAFFCA00CA0)) 
    C_OBUF_inst_i_7
       (.I0(a_IBUF[0]),
        .I1(a_IBUF[1]),
        .I2(b_IBUF[0]),
        .I3(b_IBUF[1]),
        .I4(a_IBUF[2]),
        .I5(a_IBUF[3]),
        .O(data4));
  LUT6 #(
    .INIT(64'hFEFEFEEAFEEAEAEA)) 
    C_OBUF_inst_i_8
       (.I0(seleccion_IBUF[3]),
        .I1(a_IBUF[3]),
        .I2(b_IBUF[3]),
        .I3(a_IBUF[2]),
        .I4(b_IBUF[2]),
        .I5(\Suma/cin2_out ),
        .O(C_OBUF_inst_i_8_n_0));
  LUT6 #(
    .INIT(64'h8000FFFF80000000)) 
    C_OBUF_inst_i_9
       (.I0(b_IBUF[1]),
        .I1(b_IBUF[0]),
        .I2(b_IBUF[3]),
        .I3(b_IBUF[2]),
        .I4(ALUFLAG1_IBUF),
        .I5(C_OBUF_inst_i_13_n_0),
        .O(c6));
  OBUF N_OBUF_inst
       (.I(N_OBUF),
        .O(N));
  LUT5 #(
    .INIT(32'h50101000)) 
    N_OBUF_inst_i_1
       (.I0(seleccion_IBUF[3]),
        .I1(N_OBUF_inst_i_2_n_0),
        .I2(signwire),
        .I3(N_OBUF_inst_i_4_n_0),
        .I4(awire),
        .O(N_OBUF));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    N_OBUF_inst_i_10
       (.I0(ALUFLAG1_IBUF),
        .I1(b_IBUF[0]),
        .I2(a_IBUF[0]),
        .I3(b_IBUF[1]),
        .I4(a_IBUF[1]),
        .O(\Suma/cin2_out ));
  LUT6 #(
    .INIT(64'h1E87781EE17887E1)) 
    N_OBUF_inst_i_11
       (.I0(a_IBUF[2]),
        .I1(\Resta/Carrylooktest1/cin2_out ),
        .I2(a_IBUF[3]),
        .I3(b_IBUF[2]),
        .I4(C_OBUF_inst_i_11_n_0),
        .I5(b_IBUF[3]),
        .O(s2));
  LUT6 #(
    .INIT(64'h775F77A0885F88A0)) 
    N_OBUF_inst_i_12
       (.I0(\y_OBUF[6]_inst_i_48_n_0 ),
        .I1(b_IBUF[2]),
        .I2(a_IBUF[2]),
        .I3(ALUFLAG1_IBUF),
        .I4(a_IBUF[3]),
        .I5(b_IBUF[3]),
        .O(data2));
  LUT6 #(
    .INIT(64'hFA17FAB7FFB7FAB7)) 
    N_OBUF_inst_i_2
       (.I0(seleccion_IBUF[0]),
        .I1(N_OBUF_inst_i_6_n_0),
        .I2(seleccion_IBUF[1]),
        .I3(seleccion_IBUF[2]),
        .I4(ALUFLAG1_IBUF),
        .I5(b_IBUF[3]),
        .O(N_OBUF_inst_i_2_n_0));
  LUT5 #(
    .INIT(32'h04040440)) 
    N_OBUF_inst_i_3
       (.I0(seleccion_IBUF[3]),
        .I1(Sign_IBUF),
        .I2(seleccion_IBUF[2]),
        .I3(seleccion_IBUF[1]),
        .I4(seleccion_IBUF[0]),
        .O(signwire));
  LUT6 #(
    .INIT(64'h00CA00CA00CA0FCA)) 
    N_OBUF_inst_i_4
       (.I0(N_OBUF_inst_i_7_n_0),
        .I1(N_OBUF_inst_i_8_n_0),
        .I2(seleccion_IBUF[1]),
        .I3(seleccion_IBUF[2]),
        .I4(seleccion_IBUF[0]),
        .I5(N_OBUF_inst_i_9_n_0),
        .O(N_OBUF_inst_i_4_n_0));
  LUT6 #(
    .INIT(64'h0010005000505400)) 
    N_OBUF_inst_i_5
       (.I0(seleccion_IBUF[3]),
        .I1(ALUFLAG1_IBUF),
        .I2(a_IBUF[3]),
        .I3(seleccion_IBUF[2]),
        .I4(seleccion_IBUF[1]),
        .I5(seleccion_IBUF[0]),
        .O(awire));
  LUT4 #(
    .INIT(16'h01FE)) 
    N_OBUF_inst_i_6
       (.I0(b_IBUF[2]),
        .I1(b_IBUF[1]),
        .I2(b_IBUF[0]),
        .I3(b_IBUF[3]),
        .O(N_OBUF_inst_i_6_n_0));
  LUT6 #(
    .INIT(64'h8282822882282828)) 
    N_OBUF_inst_i_7
       (.I0(seleccion_IBUF[0]),
        .I1(b_IBUF[3]),
        .I2(a_IBUF[3]),
        .I3(\Suma/cin2_out ),
        .I4(b_IBUF[2]),
        .I5(a_IBUF[2]),
        .O(N_OBUF_inst_i_7_n_0));
  MUXF7 N_OBUF_inst_i_8
       (.I0(s2),
        .I1(data2),
        .O(N_OBUF_inst_i_8_n_0),
        .S(seleccion_IBUF[0]));
  LUT6 #(
    .INIT(64'h050305FCFA03FAFC)) 
    N_OBUF_inst_i_9
       (.I0(b_IBUF[2]),
        .I1(a_IBUF[2]),
        .I2(\y_OBUF[6]_inst_i_28_n_0 ),
        .I3(ALUFLAG1_IBUF),
        .I4(a_IBUF[3]),
        .I5(b_IBUF[3]),
        .O(N_OBUF_inst_i_9_n_0));
  IBUF Sign_IBUF_inst
       (.I(Sign),
        .O(Sign_IBUF));
  OBUF V_OBUF_inst
       (.I(V_OBUF),
        .O(V));
  LUT5 #(
    .INIT(32'h00104000)) 
    V_OBUF_inst_i_1
       (.I0(seleccion_IBUF[3]),
        .I1(N_OBUF_inst_i_2_n_0),
        .I2(signwire),
        .I3(N_OBUF_inst_i_4_n_0),
        .I4(awire),
        .O(V_OBUF));
  OBUF Z_OBUF_inst
       (.I(Z_OBUF),
        .O(Z));
  LUT6 #(
    .INIT(64'h0001000100010000)) 
    Z_OBUF_inst_i_1
       (.I0(\Selector1/s__45 [2]),
        .I1(\Selector1/s__45 [3]),
        .I2(\Selector1/s__45 [0]),
        .I3(\Selector1/s__45 [1]),
        .I4(C_OBUF),
        .I5(signwire),
        .O(Z_OBUF));
  IBUF \a_IBUF[0]_inst 
       (.I(a[0]),
        .O(a_IBUF[0]));
  IBUF \a_IBUF[1]_inst 
       (.I(a[1]),
        .O(a_IBUF[1]));
  IBUF \a_IBUF[2]_inst 
       (.I(a[2]),
        .O(a_IBUF[2]));
  IBUF \a_IBUF[3]_inst 
       (.I(a[3]),
        .O(a_IBUF[3]));
  OBUF \an_OBUF[0]_inst 
       (.I(an_OBUF[0]),
        .O(an[0]));
  LUT4 #(
    .INIT(16'h0001)) 
    \an_OBUF[0]_inst_i_1 
       (.I0(seleccion_IBUF[3]),
        .I1(seleccion_IBUF[2]),
        .I2(seleccion_IBUF[0]),
        .I3(seleccion_IBUF[1]),
        .O(an_OBUF[0]));
  OBUF \an_OBUF[1]_inst 
       (.I(an_OBUF[1]),
        .O(an[1]));
  OBUF \an_OBUF[2]_inst 
       (.I(an_OBUF[1]),
        .O(an[2]));
  OBUF \an_OBUF[3]_inst 
       (.I(an_OBUF[1]),
        .O(an[3]));
  OBUF \an_OBUF[4]_inst 
       (.I(an_OBUF[1]),
        .O(an[4]));
  OBUF \an_OBUF[5]_inst 
       (.I(an_OBUF[1]),
        .O(an[5]));
  OBUF \an_OBUF[6]_inst 
       (.I(an_OBUF[1]),
        .O(an[6]));
  OBUF \an_OBUF[7]_inst 
       (.I(an_OBUF[1]),
        .O(an[7]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \an_OBUF[7]_inst_i_1 
       (.I0(seleccion_IBUF[1]),
        .I1(seleccion_IBUF[0]),
        .I2(seleccion_IBUF[2]),
        .I3(seleccion_IBUF[3]),
        .O(an_OBUF[1]));
  IBUF \b_IBUF[0]_inst 
       (.I(b[0]),
        .O(b_IBUF[0]));
  IBUF \b_IBUF[1]_inst 
       (.I(b[1]),
        .O(b_IBUF[1]));
  IBUF \b_IBUF[2]_inst 
       (.I(b[2]),
        .O(b_IBUF[2]));
  IBUF \b_IBUF[3]_inst 
       (.I(b[3]),
        .O(b_IBUF[3]));
  OBUF dp_OBUF_inst
       (.I(dp_OBUF),
        .O(dp));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    dp_OBUF_inst_i_1
       (.I0(seleccion_IBUF[2]),
        .I1(seleccion_IBUF[3]),
        .I2(seleccion_IBUF[0]),
        .I3(seleccion_IBUF[1]),
        .O(dp_OBUF));
  IBUF \seleccion_IBUF[0]_inst 
       (.I(seleccion[0]),
        .O(seleccion_IBUF[0]));
  IBUF \seleccion_IBUF[1]_inst 
       (.I(seleccion[1]),
        .O(seleccion_IBUF[1]));
  IBUF \seleccion_IBUF[2]_inst 
       (.I(seleccion[2]),
        .O(seleccion_IBUF[2]));
  IBUF \seleccion_IBUF[3]_inst 
       (.I(seleccion[3]),
        .O(seleccion_IBUF[3]));
  OBUF \y_OBUF[0]_inst 
       (.I(y_OBUF[0]),
        .O(y[0]));
  LUT4 #(
    .INIT(16'h0491)) 
    \y_OBUF[0]_inst_i_1 
       (.I0(\Selector1/s__45 [1]),
        .I1(\Selector1/s__45 [2]),
        .I2(\Selector1/s__45 [0]),
        .I3(\Selector1/s__45 [3]),
        .O(y_OBUF[0]));
  OBUF \y_OBUF[1]_inst 
       (.I(y_OBUF[1]),
        .O(y[1]));
  LUT4 #(
    .INIT(16'h408E)) 
    \y_OBUF[1]_inst_i_1 
       (.I0(\Selector1/s__45 [1]),
        .I1(\Selector1/s__45 [0]),
        .I2(\Selector1/s__45 [2]),
        .I3(\Selector1/s__45 [3]),
        .O(y_OBUF[1]));
  OBUF \y_OBUF[2]_inst 
       (.I(y_OBUF[2]),
        .O(y[2]));
  LUT4 #(
    .INIT(16'h02AE)) 
    \y_OBUF[2]_inst_i_1 
       (.I0(\Selector1/s__45 [0]),
        .I1(\Selector1/s__45 [2]),
        .I2(\Selector1/s__45 [1]),
        .I3(\Selector1/s__45 [3]),
        .O(y_OBUF[2]));
  OBUF \y_OBUF[3]_inst 
       (.I(y_OBUF[3]),
        .O(y[3]));
  LUT4 #(
    .INIT(16'h8692)) 
    \y_OBUF[3]_inst_i_1 
       (.I0(\Selector1/s__45 [0]),
        .I1(\Selector1/s__45 [1]),
        .I2(\Selector1/s__45 [2]),
        .I3(\Selector1/s__45 [3]),
        .O(y_OBUF[3]));
  OBUF \y_OBUF[4]_inst 
       (.I(y_OBUF[4]),
        .O(y[4]));
  LUT4 #(
    .INIT(16'hD004)) 
    \y_OBUF[4]_inst_i_1 
       (.I0(\Selector1/s__45 [0]),
        .I1(\Selector1/s__45 [1]),
        .I2(\Selector1/s__45 [2]),
        .I3(\Selector1/s__45 [3]),
        .O(y_OBUF[4]));
  OBUF \y_OBUF[5]_inst 
       (.I(y_OBUF[5]),
        .O(y[5]));
  LUT4 #(
    .INIT(16'hCA28)) 
    \y_OBUF[5]_inst_i_1 
       (.I0(\Selector1/s__45 [2]),
        .I1(\Selector1/s__45 [1]),
        .I2(\Selector1/s__45 [0]),
        .I3(\Selector1/s__45 [3]),
        .O(y_OBUF[5]));
  OBUF \y_OBUF[6]_inst 
       (.I(y_OBUF[6]),
        .O(y[6]));
  LUT4 #(
    .INIT(16'h2812)) 
    \y_OBUF[6]_inst_i_1 
       (.I0(\Selector1/s__45 [0]),
        .I1(\Selector1/s__45 [1]),
        .I2(\Selector1/s__45 [2]),
        .I3(\Selector1/s__45 [3]),
        .O(y_OBUF[6]));
  LUT6 #(
    .INIT(64'h72B87B2863A96A39)) 
    \y_OBUF[6]_inst_i_10 
       (.I0(seleccion_IBUF[1]),
        .I1(seleccion_IBUF[0]),
        .I2(b_IBUF[1]),
        .I3(a_IBUF[1]),
        .I4(ALUFLAG1_IBUF),
        .I5(p_0_in[0]),
        .O(\y_OBUF[6]_inst_i_10_n_0 ));
  MUXF7 \y_OBUF[6]_inst_i_11 
       (.I0(\y_OBUF[6]_inst_i_24_n_0 ),
        .I1(\y_OBUF[6]_inst_i_25_n_0 ),
        .O(\y_OBUF[6]_inst_i_11_n_0 ),
        .S(seleccion_IBUF[1]));
  LUT6 #(
    .INIT(64'h308830BB30BB3088)) 
    \y_OBUF[6]_inst_i_12 
       (.I0(data9[2]),
        .I1(seleccion_IBUF[1]),
        .I2(data8[2]),
        .I3(seleccion_IBUF[0]),
        .I4(b_IBUF[2]),
        .I5(a_IBUF[2]),
        .O(\y_OBUF[6]_inst_i_12_n_0 ));
  LUT6 #(
    .INIT(64'h7676BA2323BA8989)) 
    \y_OBUF[6]_inst_i_13 
       (.I0(seleccion_IBUF[1]),
        .I1(seleccion_IBUF[0]),
        .I2(\y_OBUF[6]_inst_i_28_n_0 ),
        .I3(ALUFLAG1_IBUF),
        .I4(a_IBUF[2]),
        .I5(b_IBUF[2]),
        .O(\y_OBUF[6]_inst_i_13_n_0 ));
  MUXF7 \y_OBUF[6]_inst_i_14 
       (.I0(\y_OBUF[6]_inst_i_29_n_0 ),
        .I1(\y_OBUF[6]_inst_i_30_n_0 ),
        .O(\y_OBUF[6]_inst_i_14_n_0 ),
        .S(seleccion_IBUF[1]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_OBUF[6]_inst_i_15 
       (.I0(\y_OBUF[6]_inst_i_31_n_0 ),
        .I1(\y_OBUF[6]_inst_i_32_n_0 ),
        .I2(seleccion_IBUF[2]),
        .I3(N_OBUF_inst_i_8_n_0),
        .I4(seleccion_IBUF[1]),
        .I5(N_OBUF_inst_i_7_n_0),
        .O(\y_OBUF[6]_inst_i_15_n_0 ));
  LUT6 #(
    .INIT(64'h0000000033E200E2)) 
    \y_OBUF[6]_inst_i_16 
       (.I0(\Suma/sum_ant[4].elemento_sumador/p__0 ),
        .I1(seleccion_IBUF[0]),
        .I2(data8[3]),
        .I3(seleccion_IBUF[1]),
        .I4(data9[3]),
        .I5(seleccion_IBUF[2]),
        .O(\y_OBUF[6]_inst_i_16_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAABAAA8)) 
    \y_OBUF[6]_inst_i_17 
       (.I0(ALUFLAG1_IBUF),
        .I1(b_IBUF[2]),
        .I2(b_IBUF[3]),
        .I3(b_IBUF[0]),
        .I4(a_IBUF[0]),
        .I5(b_IBUF[1]),
        .O(data8[0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_OBUF[6]_inst_i_18 
       (.I0(a_IBUF[3]),
        .I1(a_IBUF[1]),
        .I2(b_IBUF[0]),
        .I3(a_IBUF[2]),
        .I4(b_IBUF[1]),
        .I5(a_IBUF[0]),
        .O(\y_OBUF[6]_inst_i_18_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \y_OBUF[6]_inst_i_19 
       (.I0(b_IBUF[3]),
        .I1(b_IBUF[2]),
        .O(\y_OBUF[6]_inst_i_19_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \y_OBUF[6]_inst_i_2 
       (.I0(\y_OBUF[6]_inst_i_6_n_0 ),
        .I1(seleccion_IBUF[1]),
        .I2(\y_OBUF[6]_inst_i_7_n_0 ),
        .I3(seleccion_IBUF[2]),
        .I4(seleccion_IBUF[3]),
        .I5(\y_OBUF[6]_inst_i_8_n_0 ),
        .O(\Selector1/s__45 [0]));
  LUT6 #(
    .INIT(64'hFFFFFFEEFFFBFFEA)) 
    \y_OBUF[6]_inst_i_20 
       (.I0(b_IBUF[2]),
        .I1(b_IBUF[0]),
        .I2(b_IBUF[1]),
        .I3(b_IBUF[3]),
        .I4(\y_OBUF[6]_inst_i_36_n_0 ),
        .I5(a_IBUF[1]),
        .O(\y_OBUF[6]_inst_i_20_n_0 ));
  LUT6 #(
    .INIT(64'hB8FFFC00B800FC00)) 
    \y_OBUF[6]_inst_i_21 
       (.I0(\y_OBUF[6]_inst_i_37_n_0 ),
        .I1(a_IBUF[3]),
        .I2(\y_OBUF[6]_inst_i_38_n_0 ),
        .I3(ALUFLAG1_IBUF),
        .I4(\y_OBUF[6]_inst_i_19_n_0 ),
        .I5(\y_OBUF[6]_inst_i_39_n_0 ),
        .O(data9[1]));
  MUXF7 \y_OBUF[6]_inst_i_22 
       (.I0(P02_in[1]),
        .I1(\y_OBUF[6]_inst_i_41_n_0 ),
        .O(data8[1]),
        .S(ALUFLAG1_IBUF));
  LUT3 #(
    .INIT(8'hAC)) 
    \y_OBUF[6]_inst_i_23 
       (.I0(b_IBUF[0]),
        .I1(a_IBUF[0]),
        .I2(ALUFLAG1_IBUF),
        .O(p_0_in[0]));
  LUT6 #(
    .INIT(64'h8282822882282828)) 
    \y_OBUF[6]_inst_i_24 
       (.I0(seleccion_IBUF[0]),
        .I1(b_IBUF[1]),
        .I2(a_IBUF[1]),
        .I3(ALUFLAG1_IBUF),
        .I4(b_IBUF[0]),
        .I5(a_IBUF[0]),
        .O(\y_OBUF[6]_inst_i_24_n_0 ));
  LUT6 #(
    .INIT(64'h035CE2BDF6A91748)) 
    \y_OBUF[6]_inst_i_25 
       (.I0(seleccion_IBUF[0]),
        .I1(a_IBUF[0]),
        .I2(ALUFLAG1_IBUF),
        .I3(a_IBUF[1]),
        .I4(b_IBUF[0]),
        .I5(b_IBUF[1]),
        .O(\y_OBUF[6]_inst_i_25_n_0 ));
  MUXF7 \y_OBUF[6]_inst_i_26 
       (.I0(G02_in),
        .I1(\y_OBUF[6]_inst_i_43_n_0 ),
        .O(data9[2]),
        .S(ALUFLAG1_IBUF));
  LUT6 #(
    .INIT(64'hB8FFFC00B800FC00)) 
    \y_OBUF[6]_inst_i_27 
       (.I0(\y_OBUF[6]_inst_i_44_n_0 ),
        .I1(a_IBUF[0]),
        .I2(\y_OBUF[6]_inst_i_45_n_0 ),
        .I3(ALUFLAG1_IBUF),
        .I4(\y_OBUF[6]_inst_i_19_n_0 ),
        .I5(\y_OBUF[6]_inst_i_46_n_0 ),
        .O(data8[2]));
  LUT5 #(
    .INIT(32'hFFFACFCA)) 
    \y_OBUF[6]_inst_i_28 
       (.I0(a_IBUF[0]),
        .I1(b_IBUF[0]),
        .I2(ALUFLAG1_IBUF),
        .I3(a_IBUF[1]),
        .I4(b_IBUF[1]),
        .O(\y_OBUF[6]_inst_i_28_n_0 ));
  LUT6 #(
    .INIT(64'h8282822882282828)) 
    \y_OBUF[6]_inst_i_29 
       (.I0(seleccion_IBUF[0]),
        .I1(b_IBUF[2]),
        .I2(a_IBUF[2]),
        .I3(\Suma/cin1_in ),
        .I4(b_IBUF[1]),
        .I5(a_IBUF[1]),
        .O(\y_OBUF[6]_inst_i_29_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \y_OBUF[6]_inst_i_3 
       (.I0(\y_OBUF[6]_inst_i_9_n_0 ),
        .I1(seleccion_IBUF[3]),
        .I2(\y_OBUF[6]_inst_i_10_n_0 ),
        .I3(seleccion_IBUF[2]),
        .I4(\y_OBUF[6]_inst_i_11_n_0 ),
        .O(\Selector1/s__45 [1]));
  LUT5 #(
    .INIT(32'h606F6F60)) 
    \y_OBUF[6]_inst_i_30 
       (.I0(\y_OBUF[6]_inst_i_48_n_0 ),
        .I1(p_0_in[2]),
        .I2(seleccion_IBUF[0]),
        .I3(\Resta/Carrylooktest1/cin2_out ),
        .I4(\Resta/Carrylooktest1/p_0_in3_in ),
        .O(\y_OBUF[6]_inst_i_30_n_0 ));
  LUT4 #(
    .INIT(16'h3B7C)) 
    \y_OBUF[6]_inst_i_31 
       (.I0(ALUFLAG1_IBUF),
        .I1(seleccion_IBUF[0]),
        .I2(a_IBUF[3]),
        .I3(b_IBUF[3]),
        .O(\y_OBUF[6]_inst_i_31_n_0 ));
  LUT6 #(
    .INIT(64'hC4D0C4D0C4D09185)) 
    \y_OBUF[6]_inst_i_32 
       (.I0(seleccion_IBUF[0]),
        .I1(b_IBUF[3]),
        .I2(a_IBUF[3]),
        .I3(ALUFLAG1_IBUF),
        .I4(\y_OBUF[6]_inst_i_28_n_0 ),
        .I5(p_0_in[2]),
        .O(\y_OBUF[6]_inst_i_32_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \y_OBUF[6]_inst_i_33 
       (.I0(b_IBUF[3]),
        .I1(a_IBUF[3]),
        .O(\Suma/sum_ant[4].elemento_sumador/p__0 ));
  MUXF7 \y_OBUF[6]_inst_i_34 
       (.I0(P02_in[3]),
        .I1(\y_OBUF[6]_inst_i_52_n_0 ),
        .O(data8[3]),
        .S(ALUFLAG1_IBUF));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAABAAA8)) 
    \y_OBUF[6]_inst_i_35 
       (.I0(ALUFLAG1_IBUF),
        .I1(b_IBUF[2]),
        .I2(b_IBUF[3]),
        .I3(b_IBUF[0]),
        .I4(a_IBUF[3]),
        .I5(b_IBUF[1]),
        .O(data9[3]));
  LUT3 #(
    .INIT(8'hB8)) 
    \y_OBUF[6]_inst_i_36 
       (.I0(a_IBUF[2]),
        .I1(b_IBUF[1]),
        .I2(a_IBUF[0]),
        .O(\y_OBUF[6]_inst_i_36_n_0 ));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    \y_OBUF[6]_inst_i_37 
       (.I0(a_IBUF[2]),
        .I1(b_IBUF[0]),
        .I2(a_IBUF[3]),
        .I3(b_IBUF[1]),
        .I4(a_IBUF[1]),
        .O(\y_OBUF[6]_inst_i_37_n_0 ));
  LUT6 #(
    .INIT(64'hFFEFFFEEFFEBFFEA)) 
    \y_OBUF[6]_inst_i_38 
       (.I0(b_IBUF[2]),
        .I1(b_IBUF[0]),
        .I2(b_IBUF[1]),
        .I3(b_IBUF[3]),
        .I4(a_IBUF[1]),
        .I5(a_IBUF[2]),
        .O(\y_OBUF[6]_inst_i_38_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \y_OBUF[6]_inst_i_39 
       (.I0(a_IBUF[2]),
        .I1(b_IBUF[0]),
        .I2(a_IBUF[3]),
        .I3(b_IBUF[1]),
        .I4(a_IBUF[1]),
        .O(\y_OBUF[6]_inst_i_39_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \y_OBUF[6]_inst_i_4 
       (.I0(\y_OBUF[6]_inst_i_12_n_0 ),
        .I1(seleccion_IBUF[3]),
        .I2(\y_OBUF[6]_inst_i_13_n_0 ),
        .I3(seleccion_IBUF[2]),
        .I4(\y_OBUF[6]_inst_i_14_n_0 ),
        .O(\Selector1/s__45 [2]));
  LUT6 #(
    .INIT(64'h0101010000000100)) 
    \y_OBUF[6]_inst_i_40 
       (.I0(b_IBUF[2]),
        .I1(b_IBUF[3]),
        .I2(b_IBUF[1]),
        .I3(a_IBUF[1]),
        .I4(b_IBUF[0]),
        .I5(a_IBUF[0]),
        .O(P02_in[1]));
  LUT6 #(
    .INIT(64'hFFFFFFEFFFFFFFEC)) 
    \y_OBUF[6]_inst_i_41 
       (.I0(a_IBUF[0]),
        .I1(b_IBUF[2]),
        .I2(b_IBUF[0]),
        .I3(b_IBUF[1]),
        .I4(b_IBUF[3]),
        .I5(a_IBUF[1]),
        .O(\y_OBUF[6]_inst_i_41_n_0 ));
  LUT6 #(
    .INIT(64'h0101010000000100)) 
    \y_OBUF[6]_inst_i_42 
       (.I0(b_IBUF[2]),
        .I1(b_IBUF[3]),
        .I2(b_IBUF[1]),
        .I3(a_IBUF[2]),
        .I4(b_IBUF[0]),
        .I5(a_IBUF[3]),
        .O(G02_in));
  LUT6 #(
    .INIT(64'hFFFFFFEFFFFFFFEC)) 
    \y_OBUF[6]_inst_i_43 
       (.I0(a_IBUF[3]),
        .I1(b_IBUF[2]),
        .I2(b_IBUF[0]),
        .I3(b_IBUF[1]),
        .I4(b_IBUF[3]),
        .I5(a_IBUF[2]),
        .O(\y_OBUF[6]_inst_i_43_n_0 ));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    \y_OBUF[6]_inst_i_44 
       (.I0(a_IBUF[1]),
        .I1(b_IBUF[0]),
        .I2(a_IBUF[0]),
        .I3(b_IBUF[1]),
        .I4(a_IBUF[2]),
        .O(\y_OBUF[6]_inst_i_44_n_0 ));
  LUT6 #(
    .INIT(64'hFFEFFFEEFFEBFFEA)) 
    \y_OBUF[6]_inst_i_45 
       (.I0(b_IBUF[2]),
        .I1(b_IBUF[0]),
        .I2(b_IBUF[1]),
        .I3(b_IBUF[3]),
        .I4(a_IBUF[2]),
        .I5(a_IBUF[1]),
        .O(\y_OBUF[6]_inst_i_45_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \y_OBUF[6]_inst_i_46 
       (.I0(a_IBUF[1]),
        .I1(b_IBUF[0]),
        .I2(a_IBUF[0]),
        .I3(b_IBUF[1]),
        .I4(a_IBUF[2]),
        .O(\y_OBUF[6]_inst_i_46_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \y_OBUF[6]_inst_i_47 
       (.I0(ALUFLAG1_IBUF),
        .I1(b_IBUF[0]),
        .I2(a_IBUF[0]),
        .O(\Suma/cin1_in ));
  LUT5 #(
    .INIT(32'hCAC00A00)) 
    \y_OBUF[6]_inst_i_48 
       (.I0(a_IBUF[1]),
        .I1(b_IBUF[1]),
        .I2(ALUFLAG1_IBUF),
        .I3(a_IBUF[0]),
        .I4(b_IBUF[0]),
        .O(\y_OBUF[6]_inst_i_48_n_0 ));
  LUT3 #(
    .INIT(8'hAC)) 
    \y_OBUF[6]_inst_i_49 
       (.I0(b_IBUF[2]),
        .I1(a_IBUF[2]),
        .I2(ALUFLAG1_IBUF),
        .O(p_0_in[2]));
  MUXF7 \y_OBUF[6]_inst_i_5 
       (.I0(\y_OBUF[6]_inst_i_15_n_0 ),
        .I1(\y_OBUF[6]_inst_i_16_n_0 ),
        .O(\Selector1/s__45 [3]),
        .S(seleccion_IBUF[3]));
  LUT4 #(
    .INIT(16'hA956)) 
    \y_OBUF[6]_inst_i_50 
       (.I0(b_IBUF[2]),
        .I1(b_IBUF[1]),
        .I2(b_IBUF[0]),
        .I3(a_IBUF[2]),
        .O(\Resta/Carrylooktest1/p_0_in3_in ));
  LUT6 #(
    .INIT(64'hAAAAA8080000A808)) 
    \y_OBUF[6]_inst_i_51 
       (.I0(\y_OBUF[6]_inst_i_19_n_0 ),
        .I1(a_IBUF[3]),
        .I2(b_IBUF[1]),
        .I3(a_IBUF[1]),
        .I4(b_IBUF[0]),
        .I5(\y_OBUF[6]_inst_i_53_n_0 ),
        .O(P02_in[3]));
  LUT6 #(
    .INIT(64'h8BBB8888CFFFCFFF)) 
    \y_OBUF[6]_inst_i_52 
       (.I0(\y_OBUF[6]_inst_i_54_n_0 ),
        .I1(a_IBUF[0]),
        .I2(\y_OBUF[6]_inst_i_55_n_0 ),
        .I3(\y_OBUF[6]_inst_i_56_n_0 ),
        .I4(\y_OBUF[6]_inst_i_57_n_0 ),
        .I5(\y_OBUF[6]_inst_i_19_n_0 ),
        .O(\y_OBUF[6]_inst_i_52_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \y_OBUF[6]_inst_i_53 
       (.I0(a_IBUF[0]),
        .I1(b_IBUF[1]),
        .I2(a_IBUF[2]),
        .O(\y_OBUF[6]_inst_i_53_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \y_OBUF[6]_inst_i_54 
       (.I0(a_IBUF[0]),
        .I1(a_IBUF[2]),
        .I2(b_IBUF[0]),
        .I3(a_IBUF[1]),
        .I4(b_IBUF[1]),
        .I5(a_IBUF[3]),
        .O(\y_OBUF[6]_inst_i_54_n_0 ));
  LUT4 #(
    .INIT(16'h0200)) 
    \y_OBUF[6]_inst_i_55 
       (.I0(b_IBUF[0]),
        .I1(b_IBUF[3]),
        .I2(b_IBUF[2]),
        .I3(b_IBUF[1]),
        .O(\y_OBUF[6]_inst_i_55_n_0 ));
  LUT4 #(
    .INIT(16'h0015)) 
    \y_OBUF[6]_inst_i_56 
       (.I0(b_IBUF[2]),
        .I1(b_IBUF[0]),
        .I2(b_IBUF[1]),
        .I3(b_IBUF[3]),
        .O(\y_OBUF[6]_inst_i_56_n_0 ));
  LUT5 #(
    .INIT(32'hFCBBFC88)) 
    \y_OBUF[6]_inst_i_57 
       (.I0(a_IBUF[2]),
        .I1(b_IBUF[0]),
        .I2(a_IBUF[1]),
        .I3(b_IBUF[1]),
        .I4(a_IBUF[3]),
        .O(\y_OBUF[6]_inst_i_57_n_0 ));
  LUT4 #(
    .INIT(16'h8BB8)) 
    \y_OBUF[6]_inst_i_6 
       (.I0(data8[0]),
        .I1(seleccion_IBUF[0]),
        .I2(b_IBUF[0]),
        .I3(a_IBUF[0]),
        .O(\y_OBUF[6]_inst_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h00000000B8B8F808)) 
    \y_OBUF[6]_inst_i_7 
       (.I0(\y_OBUF[6]_inst_i_18_n_0 ),
        .I1(\y_OBUF[6]_inst_i_19_n_0 ),
        .I2(ALUFLAG1_IBUF),
        .I3(\y_OBUF[6]_inst_i_20_n_0 ),
        .I4(a_IBUF[3]),
        .I5(seleccion_IBUF[0]),
        .O(\y_OBUF[6]_inst_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h3C4A88D6289C5EC2)) 
    \y_OBUF[6]_inst_i_8 
       (.I0(seleccion_IBUF[2]),
        .I1(seleccion_IBUF[1]),
        .I2(seleccion_IBUF[0]),
        .I3(b_IBUF[0]),
        .I4(a_IBUF[0]),
        .I5(ALUFLAG1_IBUF),
        .O(\y_OBUF[6]_inst_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h308830BB30BB3088)) 
    \y_OBUF[6]_inst_i_9 
       (.I0(data9[1]),
        .I1(seleccion_IBUF[1]),
        .I2(data8[1]),
        .I3(seleccion_IBUF[0]),
        .I4(b_IBUF[1]),
        .I5(a_IBUF[1]),
        .O(\y_OBUF[6]_inst_i_9_n_0 ));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
