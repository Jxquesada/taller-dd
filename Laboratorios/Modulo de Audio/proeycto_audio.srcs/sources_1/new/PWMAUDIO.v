`timescale 1ns / 1ps
module song(
input clk,reset,pause,
output logic aud_on, wire pwm)

logic [7:0] duty;
logic [14:0] sample;
logic [13:0] counter;
PWM mypwm(clk,duty,pwm);
always@(posedge clk)
begin
if(reset | pause)
    begin
    counter<=0;
    sample<=0;
    aud_on <=0;
    end
else
    begin
    aud_on<=1;
    //8000/100M
    counter <= counter + 1; 
    if(&counter)
    sample <= sample + 1;
    end
end
