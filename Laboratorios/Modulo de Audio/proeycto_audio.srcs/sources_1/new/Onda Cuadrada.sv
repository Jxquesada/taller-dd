`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 19.05.2019 23:33:03
// Design Name: 
// Module Name: Onda Cuadrada
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Onda_Cuadrada(
 input logic clk,
 input logic enable,
 input logic [11:0] frecuency,
 input logic [1:0] duty,
 output logic [9:0] data
    );
    logic [21:0] frecuency_count=0,div;
 always_comb
   begin
   if(frecuency==0) div=0;
   else
   begin
   case(duty)
1:div=frecuency*512;
2:div=frecuency*256;
3:div=frecuency*128;
default: div=frecuency*512;
endcase
end
        end
        always@(posedge clk)
        begin
        if(frecuency_count==frecuency*1024)  frecuency_count=0;
        else frecuency_count=frecuency_count + 1;
        if(frecuency_count>div) data=10'd0;
        else data=10'd1;  
        if(enable==1) data=10'd0;
        end
endmodule
