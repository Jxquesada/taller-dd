`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.05.2019 01:21:15
// Design Name: 
// Module Name: Memory256
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Memory512(
    input wire [8:0] a,
    output wire [9:0] spo
    );
    
    parameter Datafile="Notas.mem";
    
    reg[9:0] ROM[0:511];
    
    assign spo=ROM[a];
    
    initial
    begin
    $readmemh (Datafile,ROM);
    end
endmodule
