`timescale 1ns / 1ps
module song(
input logic clk,
input logic reset,
output logic [6:0] seg,
output logic dp,
output logic [7:0] an,
input logic  ps2d, ps2c,
output logic aud_on, wire pwm);
logic [7:0] duty;
logic [17:0] sample=0;
logic clka,left_key,right_key, enter_key, key_relese;
logic [3:0] Num;
Clock_divider#(2) dividir(clk,0,clka);
blk_mem_gen_0 music1(clka,sample,duty);
PWM mypwm(clk,reset,duty,pwm);
keyboard inkey(clk, reset, ps2d, ps2c,left_key, right_key, enter_key, key_relese);
 Deco7Segmentos decob(
 Num,
 seg);
 assign an=8'b11111110;
 assign dp=1;
 parameter integer 	CNTR_WIDTH 	= 32;
parameter integer	CLK_FREQUENCY_HZ		= 100000000;  
parameter integer   UPDATE_FREQUENCY_32KHZ  = 16000;   
 logic            [CNTR_WIDTH-1:0]    clk_cnt_32Khz; 
 logic        [CNTR_WIDTH-1:0]    top_cnt_32Khz = ((CLK_FREQUENCY_HZ / UPDATE_FREQUENCY_32KHZ) - 1);
 logic            tick32Khz;                // update 8Khz clock enable    
 // Generation of 32Khz clock for the VGA 1024*768 display
 always_comb
 begin
 if(left_key || right_key || enter_key || key_relese) Num=4'b1000;
 else Num=4'b0001;
 end
 always @(posedge clk) begin
     if (reset) begin
         clk_cnt_32Khz <= {CNTR_WIDTH{1'b0}};
     end
     
     else if (clk_cnt_32Khz == top_cnt_32Khz) begin
         tick32Khz     <= 1'b1;
         clk_cnt_32Khz <= {CNTR_WIDTH{1'b0}};
     end
     else begin
         tick32Khz     <= 1'b0;
         clk_cnt_32Khz <= clk_cnt_32Khz + 1'b1;
     end
 end 
always@(posedge tick32Khz)
 begin
    aud_on=1;
    //8000/100M
    if(sample==203271) sample=0;
    else sample = sample + 1;
    end 
endmodule