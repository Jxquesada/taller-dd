`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Ashot Hambardzumyan
// 
// Create Date: 11/17/2016 09:44:52 PM
// Design Name: 
// Module Name: PWM
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PWM(
input clk,
input reset,		          // Reset assertion.
input [9:0] music_data,	      // 10-bit music sample
output reg PWM_out		      // PWM output. Connect this to ampPWM.
);
      
 reg [11:0] pwm_counter = 12'd0;           // counts up to 4096 clock cycles per pwm period
// Sequential block to generate the PWM signal         
always @(posedge clk) 
begin
    if(reset)                   // Reset condition
    begin
        pwm_counter <= 0;
        PWM_out <= 0;
    end
    else 
    begin
        pwm_counter <= pwm_counter + 1;     // Increment the PWM counter
        
        if(pwm_counter >= music_data)       // Compare the PWM counter value to 10-bit digital data
            PWM_out <= 0;                   // Duty cycle of the PWM is varied depending on the input magnitude
        else
            PWM_out <= 1;
    end
end
endmodule
