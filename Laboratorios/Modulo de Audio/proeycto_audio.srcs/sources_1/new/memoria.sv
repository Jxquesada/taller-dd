`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.05.2019 02:41:54
// Design Name: 
// Module Name: memoria
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module memoria(
input wire [14:0] a,
    output wire [7:0] spo
    );
    
    parameter Datafile="pwm.txt";
    
    reg[7:0] ROM[0:32768];
    
    assign spo=ROM[a];
    
    initial
    begin
    $readmemh (Datafile,ROM);
    end
endmodule
