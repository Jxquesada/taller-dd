`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 14.05.2019 16:15:43
// Design Name: 
// Module Name: Audio
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Audio(
    input wire clk,
    input wire reset,
   input wire [2:0] sw,
    output wire cout,
    output wire pwm,
    output logic aud_on
    );
//    wire cout1;
   wire [5:0] Song,Song1; 
    logic [9:0] sample=0,sample1=0;
    logic [5:0] addres=0;
    wire [11:0] sen_frecuency;
    wire [10:0] dato_mix2, dato;
    logic [10:0] dato_bass;//dato_bass256;
    logic [31:0] velocidad, vel_count=0;
    wire [11:0] bass_frecuency;
    logic [11:0] sin_count=0;
    logic [11:0] bass_count=0;
    Memory1024#(.Datafile("sen.mem")) seno_melodic(sample,dato);
    Memory1024#(.Datafile("sen.mem")) seno_bass(sample1,dato_bass);
    Memory64 #(.Datafile("music1.mem")) music1(addres,Song1);
   Memory64  #(.Datafile("music.mem")) music(addres,Song);
   Memory_Parametric1  #(.Datafile("Notas.mem")) notes2(Song1,bass_frecuency);
   Memory_Parametric1  #(.Datafile("Notas.mem")) notes(Song,sen_frecuency);
    CarryLookAhead #(.nbits(11)) suma2channel(dato,dato_bass,0,dato_mix2,cout);
//   CarryLookAhead #(.nbits(10)) suma3channel(dato_mix,datosquare,cout1,dato_mix2,cout); 
    PWM2 mypwm(clk,reset,dato_mix2,pwm);
always_comb
begin
//sample1[0]=0;
case(sw[1:0])
0:velocidad=32'd22000000;
1:velocidad=32'd25000000;
2:velocidad=32'd27000000;
3:velocidad=32'd30000000;
endcase
end
always@(posedge clk) 
 begin
     //sen_addres
     if(addres==63) addres=0;
      if(vel_count>=velocidad)
        begin
        addres=addres +1;     
        vel_count=0;
        end
        else vel_count = vel_count + 1;
        
    aud_on=1;
    //sen_frec
    if(sample==1023) sample=0;
    if(sen_frecuency==0)
    begin
    sample=10'd766;
    sin_count=1;
    end
    if(sin_count==2986) sin_count=1;
    if(sin_count==sen_frecuency)
        begin
        sample=sample +1;
        sin_count=0;
        end
        else sin_count=sin_count + 1; 
    // sen_bass_frecuency
//     if(sample1 [9:1] ==512) sample1[9:1]=0;
//    if(bass_frecuency==0)
//    begin
//    sample1 [9:1]=9'd383;
//    bass_count=1;
//    end
//    if(bass_count==5972) bass_count=1;
//    if(bass_count==bass_frecuency*2)
//        begin
//        sample1[9:1]=sample1[9:1] +1;
//        bass_count=0;
//        end
//        else bass_count=bass_count + 1;
//        if(sw[2]==1) sample1[9:1]=9'd383;
//        dato_bass256=dato_bass/2;
  if(sample1==1023) sample1=0;
    if(bass_frecuency==0)
    begin
    sample1=10'd766;
    bass_count=1;
    end
    if(bass_count==2986) bass_count=1;
    if(bass_count==bass_frecuency)
        begin
        sample1=sample1 +1;
        bass_count=0;
        end
        else bass_count=bass_count + 1; 
         if(sw[2]==1) sample1=10'd766;
    end 
endmodule
