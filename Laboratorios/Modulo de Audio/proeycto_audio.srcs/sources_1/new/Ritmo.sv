`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.05.2019 12:16:39
// Design Name: 
// Module Name: Ritmo
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Ritmo(
input logic clk,
input logic enable,
 input logic [9:0] frecuency,
 output logic [9:0] data1
    );
    logic [9:0] frecuency_count=0,count=0,count2=0;
     always@(posedge clk)
        begin
        if(count==1024) count=0;
        else count=count+4;
        if(count2==1024) count2=0;
        else count2=count2+16;
        if(frecuency_count==frecuency)  frecuency_count=0;
        else frecuency_count=frecuency_count + 1;
        if(frecuency_count>frecuency/2) data1=10'd766;
        else data1=count2-count;
        if(enable==1) data1=10'd766;  
        end
endmodule
