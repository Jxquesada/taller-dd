`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09.05.2019 03:17:38
// Design Name: 
// Module Name: PWM_AUDIO
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PWM_AUDIO(
    input logic clk,
    input logic sw[1:0],
    output logic Pwmsd,
    output logic PWM
    );
    logic clk8k,clk25k;
    logic out,out1;
    Clock_divider #(125000) clks(clk,0,clk8k);
    Clock_divider #(2) clks1(clk,0,clk25k);
     PWM_DEFINE(clk8k, out1);
//     music tone(clk25k,sw[1],out);
     assign Pwmsd=1;
     always_comb
     begin
     if(sw[0]==1)  PWM=out;
     else PWM=out1;
     end
endmodule
