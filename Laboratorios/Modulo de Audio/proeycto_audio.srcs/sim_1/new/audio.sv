`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.05.2019 18:05:22
// Design Name: 
// Module Name: audio
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module audio_tb;
     logic clk;
   logic  reset;
   logic  [2:0] sw;
   logic cout;
   logic  pwm;
    logic aud_on;
    Audio test(.clk(clk),.reset(reset),.sw(sw),.cout(cout),.pwm(pwm),.aud_on(aud_on));
    initial begin
		clk = 1;
		forever begin
		#5 clk = ~clk;
		end
	end
	initial begin
	reset=0;
	sw=0;
	end
endmodule
