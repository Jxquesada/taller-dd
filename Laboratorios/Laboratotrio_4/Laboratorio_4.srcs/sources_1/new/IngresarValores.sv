`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Johan Chaves Zamora
// 
// Create Date: 04.04.2019 13:30:09
// Design Name: 
// Module Name: IngresarValores
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module IngresarValores(
input logic button,
input logic [7:0] Variable,
input logic [1:0] select,
input logic selectNum,
output logic [31:0] NumA,
output logic [31:0] NumB
    );
initial
begin 
NumA=32'b00000000000000000000000000000000;
NumA=32'b00000000000000000000000000000000;
end
always@(posedge button)
begin
    if(selectNum==0)
        begin
        case(select)
        0: begin
           NumA[7:0]=Variable;
           end
        1:  begin
            NumA[15:7]=Variable;
            end
        2:  begin
            NumA[23:15]=Variable;
            end   
        3:  begin
            NumA[31:23]=Variable;
            end                                 
        endcase
        end
    else
        begin
        case(select)
        0: begin
           NumB[7:0]=Variable;
           end
        1:  begin
            NumB[15:7]=Variable;
            end
        2:  begin
            NumB[23:15]=Variable;
            end   
        3:  begin
            NumB[31:23]=Variable;
            end   
        endcase
        end
end
endmodule
