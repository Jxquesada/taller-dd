`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Johan Andres Chaves Zamora
// 
// Create Date: 17.03.2019 15:37:16
// Design Name: 
// Module Name: Corrimiento
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module CorrimientoFloat (
input logic [22 :0] MantisaA1,
input logic [22 :0] MantisaB1,
input logic selectdez,
input logic [7:0] dezplazamiento,
output logic [23:0] Res,
output logic [23:0] Res1,
output logic Redondeo
    );
 integer dezplazamiento1;
 logic [23 :0] MantisaA;
 logic [23 :0] MantisaB;
 always_comb
 begin
 MantisaA=24'b100000000000000000000000;
 MantisaB=24'b100000000000000000000000;
 MantisaA[22:0]= MantisaA1;
 MantisaB[22:0]= MantisaB1;
 dezplazamiento1=dezplazamiento;
 if(dezplazamiento==0)
    begin
    Res=MantisaA >> 1;
    Res1=MantisaB >> 1;
    Redondeo=0;
    end
 else 
    begin
        if(selectdez==0)
            begin
            Res = MantisaA >> dezplazamiento1;
            Res1=MantisaB;
            Redondeo=MantisaA[dezplazamiento1-1];
            
            end
        else
            begin
            Res = MantisaB >> dezplazamiento1;   
            Res1=MantisaA;
            Redondeo=MantisaB[dezplazamiento1-1];
            end
    end
 Redondeo=((Res[0] ^ Res1[0]) &  Redondeo);
 end    
endmodule
