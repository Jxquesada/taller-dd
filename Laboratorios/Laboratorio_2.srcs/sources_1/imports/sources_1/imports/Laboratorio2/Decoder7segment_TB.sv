`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Tecnologico de Costa Rica
// Taller de Dise�o digital 
// Johan Chaves Zamora 2016062523
// Create Date: 20.02.2019 23:03:39
// Design Name: 
// Module Name: Decoder7segment_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Decoder7segment_TB(

    );
     logic [3:0] data;
     logic [6:0] segments;
     
     Decoder7segment Decoder7segment_TB(
     .data(data),
     .segments(segments)
     );
     initial
        begin
        data=4'b1111;
        #100
        data=4'b1110;
        #100
        data=4'b1000;
        #100
        data=4'b0010;
        end
endmodule
