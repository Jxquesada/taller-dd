`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Tecnologico de Costa Rica
// Taller de Dise�o digital 
// Johan Chaves Zamora 2016062523
// Create Date: 19.02.2019 19:58:21
// Design Name: 
// Module Name: Count_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Count_TB(

    );
    logic clock;
    logic reset;
    wire [3:0] count;
    Count Countest(
        .clock(clock),
        .reset(reset),
        .count(count)
        );
    initial
        begin
        reset=1;
        #75
        reset=0;
        end
    always
        begin
        #50 clock=0;
        #50 clock=1;
        end
endmodule
