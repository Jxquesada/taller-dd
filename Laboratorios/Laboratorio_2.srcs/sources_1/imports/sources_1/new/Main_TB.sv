`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Tecnologico de Costa Rica
// Taller de Dise�o digital 
// Johan Chaves Zamora 2016062523
// Create Date: 24.02.2019 19:32:47
// Design Name: 
// Module Name: Main_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module Main_TB(
    );
    logic clock;
    logic reset;
    wire [6:0] y;
    Main Maintest(
        .clock(clock),
        .reset(reset),
        .y(y)
        );
    initial
        begin
        reset=1;
        #75
        reset=0;
        end
    always
        begin
        #50 clock=0;
        #50 clock=1;
        end
endmodule
