`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Tecnologico de Costa Rica
// Taller de Dise�o digital 
// Johan Chaves Zamora 2016062523
// Create Date: 24.02.2019 18:45:46
// Design Name: 
// Module Name: Main
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module Main( 
    input wire clock,
    input wire reset,
    output logic [6:0] y 
 );
    logic [3:0] count;
    logic [3:0] data;
    logic [6:0] a;
    Count Count1( clock, reset,count);
    Decoder7segment Decotest(count,a);
    Inversor Inversotest(a,y);
endmodule
