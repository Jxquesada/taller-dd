`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 27.02.2019 12:09:14
// Design Name: 
// Module Name: MAIN_125_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MAIN_125_TB(

    );
    logic [3:0] a;
    logic [3:0] b;
    wire dp;
    wire [7:0] an;
    wire [6:0] y; 
    logic cin;
    wire cout;
    MAIN_125 MAINTEST1(
    .a(a),
    .b(b),
    .dp(dp),
    .an(an),
    .y(y),
    .cin(cin),
    .cout(cout)
    );
    initial
    begin
    a=4'b0010;
    b=4'b0010;
    cin=1'b0;
    end
endmodule
