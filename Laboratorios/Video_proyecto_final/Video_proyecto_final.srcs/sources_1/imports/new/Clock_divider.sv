`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.04.2019 18:11:19
// Design Name: 
// Module Name: Clock_divider
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module Clock_divider #(parameter nbits=2)(
input clk,
input reset,
output clk_out);
logic [32:0] r_reg=0;
wire [32:0] r_nxt;
logic clk_track;
always @(posedge clk or posedge reset)
  begin
  if (reset) 
    begin
     r_reg <= 0;
    clk_track <= 0; 
     end
 else
    begin
     if (r_nxt == nbits)	   
    begin    
     r_reg <= 0;     
    clk_track <= ~clk_track;  
     end
    else       
    r_reg <= r_nxt;
    end
    end
assign r_nxt = r_reg+1;   	      
assign clk_out = clk_track;
endmodule