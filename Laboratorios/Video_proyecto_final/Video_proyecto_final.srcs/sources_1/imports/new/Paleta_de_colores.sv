`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.05.2019 19:50:26
// Design Name: 
// Module Name: Paleta_de_colores
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Paleta_de_colores(
    input wire [5:0] Indice,
    output wire[11:0] Out_RGB
    );
    
    parameter Datafile="Paleta.mem";
    
    reg[11:0] ROM[0:63];
    
    assign Out_RGB=ROM[Indice];
    
    initial
    begin
    $readmemh (Datafile,ROM);
    end
endmodule
