`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.06.2019 00:36:14
// Design Name: 
// Module Name: Obj_index_selector
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Obj_index_selector(
    input [9:0] Xpos,
    input [9:0] Ypos,
    input [31:0] OP,
    input [31:0] O1,
    input [31:0] O2,
    input [31:0] O3,

    output [5:0] PIS//Palette index from Sprite
    );
    wire [4:0]Xp;
    wire [6:0]preXp;
    wire [3:0]Yp,X1,Y1,X2,Y2,X3,Y3,prePIS;
    wire [5:0]preYp,preX1,preY1,preX2,preY2,preX3,preY3;
    
    wire [8:0]XYp;
    wire [7:0]XY1,XY2,XY3;
    wire [3:0]PI,O1I,O2I,O3I;
    
    assign preXp=Xpos-OP[9:0];
    assign Xp=preXp>>2;
    assign preYp=Ypos-OP[19:10];
    assign Yp=preYp>>2;
    
    assign preX1=Xpos-O1[9:0];
    assign X1=preX1>>2;
    assign preY1=Ypos-O1[19:10];
    assign Y1=preY1>>2;
    
    assign preX2=Xpos-O2[9:0];
    assign X2=preX2>>2;
    assign preY2=Ypos-O2[19:10];
    assign Y2=preY2>>2;
    
    assign preX3=Xpos-O3[9:0];
    assign X3=preX3>>2;
    assign preY3=Ypos-O3[19:10];
    assign Y3=preY3>>2;
    
    assign XYp[4:0]=Xp;
    assign XYp[8:5]=Yp;
    
    assign XY1[3:0]=X1;
    assign XY1[7:4]=Y1;
    
    assign XY2[3:0]=X2;
    assign XY2[7:4]=Y2;
    
    assign XY3[3:0]=X3;
    assign XY3[7:4]=Y3;
    
    ROM512x4 #(.Datafile("BombStep.mem"))SpriteP(XYp,PI);
    ROM256x4 #(.Datafile("BombStand.mem"))SpriteO1(XY1,O1I);
    ROM256x4 #(.Datafile("BombStand.mem"))SpriteO2(XY2,O2I);
    ROM256x4 #(.Datafile("BombStand.mem"))SpriteO3(XY3,O3I);
    
    assign prePIS=(PI!=0 && OP[9:0]<=Xpos && Xpos<(OP[9:0]+128) &&  OP[19:10]<=Ypos && Ypos <(OP[19:10]+64))?PI:
                (O1I!=0  && O1[9:0]<=Xpos && Xpos<(O1[9:0]+64)  &&  O1[19:10]<=Ypos && Ypos <(O1[19:10]+64))?O1I:
                (O2I!=0 &&  O2[9:0]<=Xpos && Xpos<(O2[9:0]+64)  &&  O2[19:10]<=Ypos && Ypos <(O2[19:10]+64))?O2I:
                (O3I!=0 &&  O3[9:0]<=Xpos && Xpos<(O3[9:0]+64)  &&  O3[19:10]<=Ypos && Ypos <(O3[19:10]+64))?O3I:4'd0;
    assign PIS[3:0]=prePIS;
    assign PIS[5:4]=2'b0;
endmodule
