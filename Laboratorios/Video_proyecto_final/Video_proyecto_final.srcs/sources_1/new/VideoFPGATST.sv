`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.06.2019 13:16:51
// Design Name: 
// Module Name: VideoFPGATST
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module VideoFPGATST(
    input wire CLK_100Mhz,
    input wire [15:0]sw,
    output wire [11:0] RGB,
    output wire Vsync,
    output wire Hsync,
    output wire [3:0] Obj_Addrs,
    output wire IRQ_Req
   // output wire [31:0] datawr,
    //output wire [1:0]memrdwr
    
    );
    wire [31:0]data,data2;
    assign data[30:16]=0;
    assign data[15:0]=sw;
    assign data[31]=sw[15];

    PPU PPUtst(
    CLK_100Mhz,
    data,
    IRQ_Req,
    RGB,
    Vsync,
    Hsync,
    Obj_Addrs,
    IRQ_Req,
    datawr,
    memrdwr);
    
   

    
    

    
endmodule
