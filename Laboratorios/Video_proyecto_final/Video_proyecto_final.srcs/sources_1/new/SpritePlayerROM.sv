`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.06.2019 00:53:14
// Design Name: 
// Module Name: SpritePlayerROM
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////



module ROM512x4(
    input wire [8:0] IN_Addres,
    output wire [3:0] OUT_Data
    );
    
    parameter Datafile="Spritedata.txt";
    
    reg[3:0] ROM[0:511];
    
    assign OUT_Data=ROM[IN_Addres];
    
    initial
    begin
    $readmemh (Datafile,ROM);
    end
endmodule