`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.06.2019 10:56:41
// Design Name: 
// Module Name: ROM64x1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ROM64x1(
    input wire [5:0] IN_Addres,
    output wire  OUT_Data
    );
    
    parameter Datafile="font.txt";
    
    reg ROM[0:64];
    
    assign OUT_Data=ROM[IN_Addres];
    
    initial
    begin
    $readmemh (Datafile,ROM);
    end
endmodule

