`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 30.05.2019 13:40:14
// Design Name: 
// Module Name: PPU
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PPU(
    input  CLK_100Mhz,
    input  wire [31:0] Obj_DATA,
    input  wire IRQ_Do,
    output [11:0] RGB,
    output Vsync,
    output Hsync,
    output tri [3:0] Obj_Addrs,
    output IRQ_Req,
    output tri [31:0]Memconstant,
    output wire [1:0]memrdwr
    );
    assign Memconstant=(IRQ_Do)?32'b1:'bz;
    assign memrdwr[0]=(IRQ_Do==1&&Obj_Addrs==4'd15)?0:1;
    assign memrdwr[1]=(IRQ_Do)?0:1;
    
    
    wire [9:0]Xpos,Ypos;
    wire Von,Clk25,RefreshReg,Reg_Wr_En;
    wire [5:0] PIB,PI,PIS,PIscore;//pallette index
    wire [11:0]preRGB;
    tri  [31:0]memdata;
    wire [3:0]memdir;
    
    wire [31:0] OP,O1,O2,O3,OS,OL;
    wire [3:0]  OA;
    //modulos 
    Clock_divider Clock25(CLK_100Mhz,0,Clk25);
    VGA_Controller VGA(Clk25,Hsync,Vsync,Xpos,Ypos,Von);
    Background Background0(Xpos,Ypos,PIB);
    Paleta_de_colores collors(PI,preRGB);
    Obj_index_selector Objindex(Xpos,Ypos,OP,O1,O2,O3,PIS);
    Obj_Registers ObjData(CLK_100Mhz,RefreshReg,Reg_Wr_En,memdata,OP,O1,O2,O3,OS,OL,OA);
    Score_hearts_Stop fonts(Xpos,Ypos,OS,OL,PIscore);
    
    assign PI=(PIscore!=0)?PIscore:(PIS!=0)?PIS:PIB;
    assign RGB=(Von)?preRGB:0;
    assign memdata=(IRQ_Do)?Obj_DATA:'bz;
    assign Obj_Addrs=(IRQ_Do)?OA:'bz;
    assign IRQ_Req=(Ypos>480)?1:0;
    assign Reg_Wr_En=IRQ_Do;
    assign RefreshReg=IRQ_Do;
    
endmodule
