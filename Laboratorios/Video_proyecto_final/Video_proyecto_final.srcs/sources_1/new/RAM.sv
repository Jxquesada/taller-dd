`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09.06.2019 18:59:16
// Design Name: 
// Module Name: RAM
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module RAM(
    input  wire clk,
    input  wire MEMRD,
    input  wire MEMWR,
    input  wire [3:0] DIR,
    input  wire [31:0] DTI,
    output wire [31:0] DTO
    );
    
    reg [31:0] RAM[0:15];
    initial 
    begin
    RAM[0]=31'b0;
    RAM[1]=31'b0;
    RAM[2]=31'b0;
    RAM[3]=31'b0;
    RAM[4]=31'b0;
    RAM[5]=31'b0;
    RAM[6]=31'b0;
    RAM[7]=31'b0;
    RAM[8]=31'b0;
    RAM[9]=31'b0;
    RAM[10]=31'b0;
    RAM[11]=31'b0;
    RAM[12]=31'b0;
    RAM[13]=31'b0;
    RAM[14]=31'b0;
    RAM[15]=31'b0;
    end
    
    assign DTO=(MEMRD)?0:RAM[DIR];
    always @(posedge clk)
    begin
    if(~MEMWR)
    RAM[DIR]=DTI;
    end
endmodule
