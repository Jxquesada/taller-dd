`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05.06.2019 17:36:59
// Design Name: 
// Module Name: PPUtest
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PPUtest(

    );
    
    
    logic CLK_100Mhz=0;
    wire [11:0] RGB;
    wire Vsync;
    wire Hsync;
    wire [3:0] Obj_Addrs;
    wire IRQ_Req;
    

    PPU UUT(
    CLK_100Mhz,
    32'b0,
    1'b0,
    RGB,
    Vsync,
    Hsync,
    Obj_Addrs,
    IRQ_Req);
    
    always#5 CLK_100Mhz=~CLK_100Mhz;
endmodule
