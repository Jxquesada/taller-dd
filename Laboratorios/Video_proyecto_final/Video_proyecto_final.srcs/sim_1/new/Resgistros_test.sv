`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.06.2019 20:09:43
// Design Name: 
// Module Name: Resgistros_test
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Resgistros_test(

    );
    logic clk=0;
    wire Refresh;
    wire WR_En;
    wire [31:0] Obj_data;
    
     logic [31:0] Obj_player;
     logic [31:0] Obj_1;
     logic [31:0] Obj_2;
     logic [31:0] Obj_3;
     logic [31:0] Obj_score;
     logic [31:0] Obj_live;
     logic [3:0] Obj_addres;
        
   Obj_Registers UUT(clk,
   1,        
   1,         
   Obj_data,
     Obj_player,
     Obj_1,
     Obj_2     ,
     Obj_3     ,
     Obj_score, 
     Obj_live,  
    Obj_addres 
   );
   logic [19:0]counter;
   initial begin
   counter=0;
   end
   always #5 clk=~clk;
   always @(posedge clk)
   begin
   counter=counter+1;
   end
   assign Obj_data=counter;
endmodule
