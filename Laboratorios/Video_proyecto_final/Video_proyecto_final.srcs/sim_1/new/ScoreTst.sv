`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09.06.2019 13:32:32
// Design Name: 
// Module Name: ScoreTst
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ScoreTst(

    );
    logic  [31:0]Xpos=0;  
     logic [31:0]Ypos=0; 
    logic   [31:0]OS=31'h76543210;    
     logic  [31:0]OL=31'h80000002;   
     wire [5:0] BG2I ;  
    
    Score_hearts_Stop UUT(
     Xpos,
    Ypos,
     OS, 
     OL, 
     BG2I
    );
    
    logic clk=0;
    always #5 clk=~clk;
    always @(posedge clk)
    begin
    if (Xpos==799)begin
        Xpos=0;
        if(Ypos==524)Ypos=01;
        else Ypos=Ypos+1;end
    else Xpos=Xpos+1;
    
    end
endmodule
