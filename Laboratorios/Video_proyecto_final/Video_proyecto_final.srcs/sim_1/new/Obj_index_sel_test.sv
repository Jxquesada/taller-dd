`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.06.2019 20:07:28
// Design Name: 
// Module Name: Obj_index_sel_test
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Obj_index_sel_test(

    );
    logic clk=0;
   logic [9:0] Xpos=0;
logic [9:0] Ypos=0;
logic [31:0] OP=100 ;
logic [31:0] O1=300 ;
logic [31:0] O2=400 ;
logic [31:0] O3=500 ;
logic ap/*,a1,a2,a3*/;
wire [5:0] PIS;
wire [9:0] OPcheck;
   Obj_index_selector UUT(
    .Xpos(Xpos),
    .Ypos(Ypos),
     .OP(OP) ,
     .O1(O1) ,
     .O2(O2) ,
     .O3(O3) ,

    .PIS(PIS)//Palette index from Sprite
    );
    
    assign OPcheck=OP[9:0];
always#5 clk=~clk;    
    
    always@(posedge clk)
    begin
        if ((OP[9:0]<=Xpos<(OP[9:0]+128)) &&  (OP[19:10]<=Ypos<(OP[19:10]+64)))
        begin
            ap=1;end
            else ap=0;
        if (Xpos==799)begin Xpos=0; Ypos=Ypos+1;end
        else Xpos=Xpos+1;
        if (Ypos>=524)Ypos<=0;
    end
endmodule
