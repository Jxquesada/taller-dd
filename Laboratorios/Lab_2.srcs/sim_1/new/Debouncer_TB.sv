`timescale 1us / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 28.02.2019 00:33:44
// Design Name: 
// Module Name: Debouncer_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Debouncer_TB(
    );
    logic ck;
    logic on;
    logic bt;
    Debouncer_top test(
    .clk(ck),
    .btn_in(on),
    .btn_out(bt)
    );
    initial
    begin
    on=1;
    #20000
    on=0;
    #1000
    on=1;

    end
    always
    begin
    #2 ck=0;
    #2 ck=1;
    end
endmodule
