----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 25.02.2019 21:24:14
-- Design Name: 
-- Module Name: FullAdder4Bits_TB - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity FullAdder4Bits_tb is
end;

architecture bench of FullAdder4Bits_tb is

  component FullAdder4Bits
      Port ( A : in STD_LOGIC_VECTOR (3 downto 0);
             B : in STD_LOGIC_VECTOR (3 downto 0);
             Cin : in STD_LOGIC;
             Sum : out STD_LOGIC_VECTOR (3 downto 0);
             Cout : out STD_LOGIC);
  end component;

  signal A: STD_LOGIC_VECTOR (3 downto 0);
  signal B: STD_LOGIC_VECTOR (3 downto 0);
  signal Cin: STD_LOGIC;
  signal Sum: STD_LOGIC_VECTOR (3 downto 0);
  signal Cout: STD_LOGIC;

begin

  uut: FullAdder4Bits port map ( A    => A,
                                 B    => B,
                                 Cin  => Cin,
                                 Sum  => Sum,
                                 Cout => Cout );

  stimulus: process
  begin
  
    -- Put initialisation code here
    A <= "0000";
    B <= "0000";
    Cin <= '0';

    -- Put test bench stimulus code here
    wait for 100ns;
    A <= "1000"; B <= "1000"; Cin <= '0';
    wait for 100ns;
    A <= "0010"; B <= "0001"; Cin <= '0';
    wait for 100ns;
    A <= "0010"; B <= "0001"; Cin <= '1';
    wait for 100ns;
    A <= "1110"; B <= "0001"; Cin <= '1';
    wait for 100ns;
    A <= "1111"; B <= "1111"; Cin <= '1';
    wait;
  end process;


end;
