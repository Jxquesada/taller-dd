----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 25.02.2019 13:22:39
-- Design Name: 
-- Module Name: FullAdder1Bit_TB - Behavioral_TB
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.Numeric_Std.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FullAdder1Bit_TB is
    
end FullAdder1Bit_TB;

architecture Behavioral_TB of FullAdder1Bit_TB is

    component FullAdder1Bit
        Port ( A : in STD_LOGIC;
               B : in STD_LOGIC;
               Cin : in STD_LOGIC;
               S : out STD_LOGIC;
               Cout : out STD_LOGIC);
    end component;
    
  signal A: STD_LOGIC;
  signal B: STD_LOGIC;
  signal Cin: STD_LOGIC;
  signal S: STD_LOGIC;
  signal Cout: STD_LOGIC;
begin

  uut: FullAdder1Bit port map ( A    => A,
                                B    => B,
                                Cin  => Cin,
                                S    => S,
                                Cout => Cout );

  stimulus: process
  begin
  
    -- Put initialisation code here
    A <= '0';
    B <= '0';
    Cin <= '0';

    -- Put test bench stimulus code here
    wait for 100ns;
    A <= '0'; B <= '0'; Cin <= '1';
    wait for 100ns;
    A <= '0'; B <= '1'; Cin <= '0';
    wait for 100ns;
    A <= '0'; B <= '1'; Cin <= '1';
    wait for 100ns;
    A <= '1'; B <= '0'; Cin <= '0';
    wait for 100ns;
    A <= '1'; B <= '0'; Cin <= '1';
    wait for 100ns;
    A <= '1'; B <= '1'; Cin <= '0';
    wait for 100ns;
    A <= '1'; B <= '1'; Cin <= '1';
    wait;
  end process;


end;
