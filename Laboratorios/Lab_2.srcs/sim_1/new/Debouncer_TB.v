`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 27.02.2019 21:44:00
// Design Name: 
// Module Name: Debouncer_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Debouncer_TB;
 // Inputs
 logic pb_1;
 logic clk;
 // Outputs
 wire pb_out;
 // Instantiate the debouncing Verilog code
 debouncer uut (
  .pb_1(pb_1), 
  .clk(clk), 
  .pb_out(pb_out)
 );
 initial begin
  clk = 0;
  forever #100 clk = ~clk;
 end
 initial begin
  pb_1 = 0;
  #100;
  pb_1=1;
  #50;
  pb_1 = 0;
  #100;
  pb_1=1;
  #100
  pb_1=0;
  
 end 
      
endmodule
