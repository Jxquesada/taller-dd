`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 25.02.2019 20:41:43
// Design Name: 
// Module Name: Deco7Segmentos_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Deco7Segmentos_TB(

    );
    logic [3:0] Num;
    logic [6:0] Led;
    
    
    Deco7Segmentos Deco7Segmentos_TB(
    .Num(Num),
    .Led(Led));
    initial
        begin
        Num=4'b1111;
        #100
        Num=4'b1110;
        #100
        Num=4'b1000;
        #100
        Num=4'b0010;
        end
endmodule
