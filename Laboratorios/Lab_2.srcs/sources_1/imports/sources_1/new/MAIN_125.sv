`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 27.02.2019 12:01:29
// Design Name: 
// Module Name: MAIN_125
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MAIN_125(
    input wire [3:0] a,
    input wire [3:0] b,
    output logic dp,
    output logic [7:0] an,
    output logic cout,
    output wire [6:0] y, 
    input wire cin
    );
    logic [6:0] c;
    logic [3:0] s;
    FULLADDER4B Fulladder(a,b,cin,s,cout);
    Decoder7segment Decotest(s,c);
    Inversor Inversotest(c,y);
    initial
    begin
    an=8'b11111110;
    dp=1;
    end
endmodule

