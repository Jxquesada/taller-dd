`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Tecnologico de Costa Rica
// Taller de Dise�o digital 
// Johan Chaves Zamora 2016062523
// Create Date: 24.02.2019 18:45:46
// Design Name: 
// Module Name: Main
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module Main( 
    input wire ck,
    input logic on,
    input wire reset,
    output logic dp,
    output logic [7:0] an,
    output logic [6:0] y
 );
    logic [3:0] count;
    logic [3:0] data;
    logic [6:0] a;
    logic bt;
    logic rs;
    Debouncer_top testdeb(ck,on,bt);
    Debouncer_top testdeb1(ck,reset,rs);
    Count Count1(bt,rs,count);
    Decoder7segment Decotest(count,a);
    Inversor Inversotest(a,y);
    initial
    begin
        an=8'b11111110;
        dp=1;
        end
    always @ (posedge ck)
    begin  
    if(reset==0)
        begin
        an=8'b11111110;
        dp=1;
        end
      else
        begin
        an=8'b11111111;
        dp=0;
        end
     end
endmodule
