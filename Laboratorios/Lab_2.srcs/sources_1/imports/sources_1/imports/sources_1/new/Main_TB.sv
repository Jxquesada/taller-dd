`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Tecnologico de Costa Rica
// Taller de Dise�o digital 
// Johan Chaves Zamora 2016062523
// Create Date: 24.02.2019 19:32:47
// Design Name: 
// Module Name: Main_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module Main_TB(
    );
    logic ck;
    logic on;
    logic reset;
    wire [6:0] y;
    logic dp;
    logic [7:0] an;
    logic bt;
    Main Maintest(
        .on(on),
        .ck(ck),
        .reset(reset),
        .y(y),
        .an(an),
         .bt(bt),
        .dp(dp)
        );
    initial
        begin
        reset=1;
        #750
        reset=0;
        #200
        on=1;
        #10000
        on=0;
        end
    always
        begin
        #200 ck=0;
        #2000 ck=1;
        end
endmodule
