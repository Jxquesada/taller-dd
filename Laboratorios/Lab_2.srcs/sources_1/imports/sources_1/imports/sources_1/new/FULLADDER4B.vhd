----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 21.02.2019 12:38:43
-- Design Name: 
-- Module Name: FULLADDER4B - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FULLADDER4B is
port(a, b: in STD_LOGIC_VECTOR(3 downto 0);
cin: in STD_LOGIC;
s: out STD_LOGIC_VECTOR(3 downto 0);
cout: out STD_LOGIC);
end FULLADDER4B;

architecture struct of FULLADDER4B is
component FULLADDER
port(a, b, cin: in STD_LOGIC;
s, cout: out STD_LOGIC);
end component;
signal cout1, cout2,cout3: STD_LOGIC;
begin
F1: FULLADDER port map(a(0), b(0), cin, s(0),cout1);

F2: FULLADDER port map(a(1), b(1), cout1, s(1),cout2);

F3: FULLADDER port map(a(2), b(2),cout2, s(2), cout3);

F4: FULLADDER port map(a(3), b(3), cout3, s(3), cout);

end; 

