----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 21.02.2019 12:40:06
-- Design Name: 
-- Module Name: FULLADDER4B_TB - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FULLADDER4B_TB is
--  Port ( );
end FULLADDER4B_TB;

architecture Behavioral of FULLADDER4B_TB is
component FULLADDER4B
port(a, b: in STD_LOGIC_VECTOR(3 downto 0);
cin: in STD_LOGIC;
s: out STD_LOGIC_VECTOR(3 downto 0);
cout: out STD_LOGIC);
end component;

--Inputs
SIGNAL a :STD_LOGIC_VECTOR(3 downto 0) := "1001";

SIGNAL b :STD_LOGIC_VECTOR(3 downto 0) := "0010";

SIGNAL cin :STD_LOGIC := '0';

--OUTPUTS
SIGNAL cout :STD_LOGIC;

SIGNAL s :STD_LOGIC_VECTOR(3 downto 0);

begin
    TEST: FULLADDER4B PORT MAP(
    a=> a,
    b=> b,
    cin=>cin,
    cout=>cout,
    s=> s
    );
    
    TB : process
   begin
    wait for 100ns;
    a<="0100";
    b<="0101";
    wait for 100ns;
    a<="0110";
    b<="1101";
    wait;
    end process;

end Behavioral;
