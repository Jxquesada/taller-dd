----------------------------------------------------------------------------------
-- Company: ITCR
-- Engineer: 
-- JORDY QUESADA AVENDA�O 2014160028
-- Create Date: 25.02.2019 12:12:36
-- Design Name: 
-- Module Name: FullAdder4Bits - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FullAdder4Bits is
    Port ( A : in STD_LOGIC_VECTOR (3 downto 0);
           B : in STD_LOGIC_VECTOR (3 downto 0);
           Cin : in STD_LOGIC;
           Sum : out STD_LOGIC_VECTOR (3 downto 0);
           Cout : out STD_LOGIC);
end FullAdder4Bits;

architecture Behavioral of FullAdder4Bits is

    component FullAdder1Bit
        Port ( A : in STD_LOGIC;
               B : in STD_LOGIC;
               Cin : in STD_LOGIC;
               S : out STD_LOGIC;
               Cout : out STD_LOGIC);
    end component;

    signal Caux1,Caux2,Caux3 : STD_LOGIC;

begin

    INST_FullAdder0 : FullAdder1Bit PORT MAP
        (A => A(0), B => B(0),Cin => Cin, Cout => Caux1, S => Sum(0));
    INST_FullAdder1 : FullAdder1Bit PORT MAP
        (A => A(1), B => B(1),Cin => Caux1, Cout => Caux2, S => Sum(1));
    INST_FullAdder2 : FullAdder1Bit PORT MAP
        (A => A(2), B => B(2),Cin => Caux2, Cout => Caux3, S => Sum(2));
    INST_FullAdder3 : FullAdder1Bit PORT MAP
        (A => A(3), B => B(3),Cin => Caux3, Cout => Cout, S => Sum(3));
        

end Behavioral;
