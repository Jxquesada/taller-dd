----------------------------------------------------------------------------------
-- Company: ITCR
-- Engineer: 
-- JORDY QUESADA AVENDA�O 2014160028
-- Create Date: 25.02.2019 12:12:36
-- Design Name: 
-- Module Name: FullAdder1Bit - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FullAdder1Bit is
    Port ( A : in STD_LOGIC;
           B : in STD_LOGIC;
           Cin : in STD_LOGIC;
           S : out STD_LOGIC;
           Cout : out STD_LOGIC);
end FullAdder1Bit;

architecture Behavioral of FullAdder1Bit is
signal AxoB : STD_LOGIC;

begin
AxoB <= A xor B;
S <= AxoB xor Cin;
Cout <= (A nand B) nand (AxoB nand Cin);
end Behavioral;
