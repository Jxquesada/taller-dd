`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: ITCR
// Engineer:  Jordy Quesada Avenda�o
// 
// Create Date: 27.04.2019 15:34:41
// Design Name: 
// Module Name: VGA_Sprite_SET
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 


/*modulo que incorpora el uso del controllador vga y realiza 
imprecion de colores aleatorios por medio de un boton ademas de 
realizar un dezplazamiento animado de un sprite al rededor del cuadro que se
pintar� con el color aleatorio
*/

// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module VGA_Sprite_SET(
     input wire clk100,
     input wire IN_Btn,
    input wire IN_Reset,
    //input wire reset,//tb reset eliminar implementacion
    output wire OUT_Vsync,
    output wire OUT_Hsync,
    output wire R,G,B,
    output [11:0] OUT_RGB
    );
    parameter   xStretch=2,
                yStretch=2;
                
    wire        clk_25MHz,
                xy_inrange,
                VideoOn,
                moveSprite,
                Nullpix,
                clk_xStretch,
                clk_yStretch,
                Btn,
                Btnr;
                
    wire [1:0]  RotateState,
                SquareState;
    
    wire [3:0]  X,
                Y;
        
    wire [5:0]  RRGB,
            RLRGB,
            LadrilloPaleta,
            SELECTRGB
    ;
    
    wire [7:0]  XYaddr,
                LAddres;
    
    wire [9:0]  Xpos,
                Ypos,
                xSprite,
                ySprite;
                
    wire [11:0] RGB,
                SRGB,
                R12RGB,
                LCollor,
                RGBLED,
                BGRGB;            
 /////////////BOTON Reset///////////
Debouncer_top Botonr(clk100,IN_Reset,Btnr);//Random
///////////////
 Clock_divider#(2) clk25(clk100,0,clk_25MHz);//cambiar reset a 0 en implementacion
// Clock_divider#(50000) clk2(clk100,0,clk_2K);
 VGA_Controller VGA_SyncData(clk_25MHz,OUT_Hsync,OUT_Vsync,Xpos,Ypos,VideoOn);
 Sprite_potition_comparer#(/*Xstretch=*/xStretch,/*Ystretch=*/yStretch)principalcomparer(
    Xpos,
    Ypos,
    xSprite,
    ySprite,
    xy_inrange,/*Outs*/
    moveSprite
    );
    
Sprite_movement#(/*Xstretch=*/xStretch,/*Ystretch=*/yStretch)Dezplazamiento(
    SquareState,
    moveSprite,
    xSprite,/*outputs*/
    ySprite,
    RotateState
    );
/////////////BOTON Random///////////
Debouncer_top Boton(clk100,IN_Btn,Btn);//Random
///////////////
Random_color_FSM SquareDrawer(
    Btn,
    clk100,
    Btnr,
    Xpos,
    Ypos,
    SquareState,/*outputs*/
    RRGB,
    RLRGB
    );
    
Rotador_sprite Rotate(
    X,
    Y,
    RotateState,
    XYaddr/*Out*/
    );
        
    
Animation_Sprite Gif(
    moveSprite,//clk
    Btnr,//Reset
    XYaddr,
    SELECTRGB,/*outs*/
    Nullpix);
Paleta_de_colores  #(.Datafile("Paleta.mem")) PaletaSpr(SELECTRGB,SRGB);
//////////////XADDRES///////////////////////
//valores para Addres X y Addres Y del ROM

    


Extirador_byShift#(/*xstretch=*/xStretch,/*ystretch*/yStretch)ZoomSprite(
    Xpos,
    Ypos,
    xSprite,
    ySprite,
    X,
    Y
    );


ROM256x6 #(.Datafile("Ladrillos.mem")) Ladrillo(LAddres,LadrilloPaleta);
assign LAddres[3:0]=Xpos[3:0];
assign LAddres[7:4]=Ypos[3:0]; 

 Paleta_de_colores  #(.Datafile("Paleta.mem")) PaletaLa(LadrilloPaleta,LCollor);
//seccion de selector de color 
//assign R12RGB[0]=0;             assign R12RGB[4:3]=0;           assign R12RGB[8:7] =0;           assign R12RGB[11]=0;
//assign R12RGB[2:1]=RRGB[1:0];   assign R12RGB[6:5]=RRGB[3:2];   assign R12RGB[10:9]=RRGB[5:4];
Paleta_de_colores  #(.Datafile("Paleta.mem")) Paleta(RRGB,R12RGB);
Paleta_de_colores  #(.Datafile("Paleta.mem")) Paleta2(RLRGB,RGBLED);
 LED_RGB LED(clk_25MHz,RGBLED, R, G,B);
 assign BGRGB=(Xpos<16 || (312<=Xpos&&Xpos<328) || Xpos>=624 || (232<=Ypos&& Ypos<248)|| Ypos<16 || Ypos>=464)?LCollor:R12RGB;
assign RGB=(xy_inrange || Nullpix)?BGRGB:SRGB;
assign OUT_RGB=(VideoOn)?RGB:0;

/////////////////
endmodule
