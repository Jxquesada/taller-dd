`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 26.04.2019 23:39:21
// Design Name: 
// Module Name: Sprite_movement
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Sprite_movement#(parameter Xstretch=1,Ystretch=1)(
    input   wire [1:0] IN_SquareState,
    input   wire IN_Move,
    output  wire [9:0] OUT_xSprite,
    output  wire [9:0] OUT_ySprite,
    output  wire [1:0] RotateState
    );
    

localparam SXP=312-16*Xstretch;  //posicion x inicial Starting X Potition
localparam SYP=232-16*Ystretch;  //posicion y inicial Starting Y Potition
localparam JX=312; //Jump hacia derecha
localparam JY=232; // Jump hacia abajo
localparam TW=320-24; //Travel Width: Cantidad de dezplazamiento a la horizontal
localparam TH=240-24; //Travel Height: cantidad de dezplazamineto hacia vertical
localparam SW=16;   //Sprite Width
localparam SH=16;   //Sprite Height
localparam speed=1; //cantidad de pixel por fotograma a la que se dezplaza el sprite

localparam Estado0=0;//1
localparam Estado1=1;//2
localparam Estado2=2;//3
localparam Estado3=3;//4

logic [9:0] xSprite;
logic [9:0] ySprite;
logic [1:0] State;
initial
begin
  xSprite=SXP;
  ySprite=SYP;
  State=0;      
end

assign RotateState=State;

assign OUT_xSprite=
            (IN_SquareState==0 || IN_SquareState==2)?xSprite:xSprite+JX;
            
assign OUT_ySprite=
            (IN_SquareState==0 || IN_SquareState==1)?ySprite:ySprite+JY;


always@(posedge IN_Move)
begin
case(State)
0:      begin
        xSprite<=xSprite-speed;
        if(xSprite<=(SXP+Xstretch*SW-TW+1))
            begin
            State<=Estado1;
            end
        else
            State<=Estado0;
        end
        
1:      begin
        ySprite<=ySprite-speed;
        if(ySprite<=(SYP+Ystretch*SH-TH+1))
            begin
            State<=Estado2;
            end
        else
            State<=Estado1;
        end
    
2:      begin
        xSprite<=xSprite+speed;
        if(xSprite>=SXP-1)
            begin
            State<=Estado3;
            end
        else
            State<=Estado2;
        end
    
3:      begin
        ySprite<=ySprite+speed;
        if(ySprite>=SYP-1)
            begin
            State<=Estado0;
            end
        else
            State<=Estado3;
        end
        
 default:
        begin
        State<=Estado0;
        end
        
    endcase
    
 end
 
endmodule
