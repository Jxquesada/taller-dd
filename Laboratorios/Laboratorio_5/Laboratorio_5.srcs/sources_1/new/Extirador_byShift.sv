`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 28.04.2019 22:06:29
// Design Name: 
// Module Name: Extirador_byShift
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Extirador_byShift#(parameter xstretch=2,ystretch=2)(
    input [9:0] IN_Xpos,
    input [9:0] IN_Ypos,
    input [9:0] IN_xSprite,
    input [9:0] IN_ySprite,
    output [3:0] OUT_X,
    output [3:0] OUT_Y
    );
    wire [9:0] xResult;
    wire [9:0] yResult;
    assign xResult=(IN_Xpos-IN_xSprite)>>xstretch-1;
    assign yResult=(IN_Ypos-IN_ySprite)>>ystretch-1;
    assign OUT_X=xResult[3:0];
    assign OUT_Y=yResult[3:0];
endmodule
