`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 26.04.2019 13:47:21
// Design Name: 
// Module Name: Counter_2Param
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// Este contador alcanza el valor de Maxcount y vuelve a cero
module Counter_2Params #(parameter Nbits=2,Maxcount=3)( // 0 <= Nbits // 0 < Maxcount < 2^Nbits
    input IN_clk,
    input IN_reset,
    output logic [Nbits-1:0] OUT_count
    );
    
    initial
        begin
        OUT_count<=0;
        end
        
    always@(posedge IN_clk)
        begin
        if(IN_reset)
            OUT_count<=0;
        else if(OUT_count==Maxcount)
            OUT_count<=0;
        else
            OUT_count<=OUT_count+1;
        end
endmodule
