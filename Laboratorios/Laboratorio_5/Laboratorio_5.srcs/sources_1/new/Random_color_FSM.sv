`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 27.04.2019 13:35:27
// Design Name: 
// Module Name: Random_color_FSM
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Random_color_FSM(
    input   wire IN_Btn,
    input   wire IN_Clk,
    input   wire IN_Reset,
    input   wire [9:0] IN_xpos,
    input   wire [9:0] IN_ypos,
    output  wire [1:0] OUT_State,
    output  wire [5:0] OUT_Rgb,
      output  wire [5:0] Out_rgbled

    );
localparam Estado0=0;//1
localparam Estado1=1;//2
localparam Estado2=2;//3
localparam Estado3=3;//4

logic Btn;
wire [5:0]RGB;


reg [1:0]state;
logic [5:0]RGB0;
logic [5:0]RGB1;
logic [5:0]RGB2;
logic [5:0]RGB3;
logic [5:0]RGB4;


Counter_2Params #(/*Nbits=*/6,/*Maxcoutn*/63)Randomcolor(IN_Clk,IN_Reset,RGB);


//////////////////

assign OUT_State=state;
assign OUT_Rgb  =   (IN_xpos<320    &&  IN_ypos<240  )? RGB0:
                    (IN_xpos>=320   && IN_ypos<240  )?  RGB1:
                    (IN_xpos<320    && IN_ypos>=240 )?  RGB2:
                                                        RGB3;
 assign Out_rgbled=RGB0;

assign Btn=(IN_Btn^IN_Reset);

initial
    begin
    state=Estado0;
    RGB0=6'h3f;
    RGB1=6'h3f;
    RGB2=6'h3f;
    RGB3=6'h3f;
    end
    

always@(posedge Btn)
    begin
        if (IN_Reset)
            begin
            RGB0=6'h3f;
            RGB1=6'h3f;
            RGB2=6'h3f;
            RGB3=6'h3f;
            state=Estado0;
            end
        else
            begin
//////////////////
        case(state)
        
    0:      begin
            if(~IN_Reset)
                RGB0<=RGB;
            state=Estado1;
            end
            
    1:      begin
            if(~IN_Reset)
                RGB1<=RGB;
            state=Estado2;
            end
            
    2:      begin
            if(~IN_Reset)
                RGB2<=RGB;
            state=Estado3;
            end
            
    3:      begin
            if(~IN_Reset)
                RGB3<=RGB;
            state=Estado0;
            end
            
    default:begin
            state=Estado0;
            end
        endcase
 //////////////////
            end

    
    end
endmodule
