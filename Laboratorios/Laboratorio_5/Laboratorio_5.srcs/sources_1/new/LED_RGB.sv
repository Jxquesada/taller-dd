`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.05.2019 20:06:31
// Design Name: 
// Module Name: LED_RGB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module LED_RGB(
     input wire clk,
     input logic [11:0] IN_RGB,
    output logic R,
    output logic G,
    output logic B
    );
    integer count=0;
    always@(posedge clk)
     begin
       R=IN_RGB[count+8];
       G=IN_RGB[count+4];
       B=IN_RGB[count];
    if(count==3)
        count=0;
     else
     count=count+1;
 end
endmodule
