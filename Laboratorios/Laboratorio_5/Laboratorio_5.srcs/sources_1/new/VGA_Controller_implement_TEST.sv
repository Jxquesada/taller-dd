`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.04.2019 16:14:58
// Design Name: 
// Module Name: VGA_Controller_implement_TEST
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module VGA_Controller_implement_TEST(
    input   wire    clk100,
//    input wire reset,
    input wire [5:0] sw,
    output  wire    OUT_Hsync,
    output  wire    OUT_Vsync,
    output  wire    [11:0] OUT_RGB ,
    output wire R,G,B
    );
    wire    [9:0] Xpos;
    wire    [9:0] Ypos;
    wire    VideoOn;
    wire clk_25MHz;
    wire [11:0] LCollor;
  Paleta_de_colores  #(.Datafile("Paleta.mem")) Paleta(sw,LCollor);
 Clock_divider#(2) clk25_1(clk100,0,clk_25MHz);
 VGA_Controller VGA_SyncData(clk_25MHz,OUT_Hsync,OUT_Vsync,Xpos,Ypos,VideoOn);
 LED_RGB LED( clk_25MHz,LCollor, R, G,B);
assign OUT_RGB = VideoOn==1? LCollor:12'b0;     
endmodule
