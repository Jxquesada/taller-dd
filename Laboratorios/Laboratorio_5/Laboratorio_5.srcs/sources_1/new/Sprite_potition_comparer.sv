`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 26.04.2019 22:28:45
// Design Name: 
// Module Name: Sprite_potition_comparer
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Sprite_potition_comparer#(parameter Xstretch=1,Ystretch=1)(
    input   wire [9:0]  IN_xpos,
    input   wire [9:0]  IN_ypos,
    input   wire [9:0]  IN_xSprite,
    input   wire [9:0]  IN_ySprite,
    output  wire        OUT_xy_inrange,
    output  wire        OUT_moveSprite
    );
    
    localparam SW=16; //SW:sprite width
    localparam SH=16; //SH:sprite Height
    localparam  VD = 480;    //area de visualizacion vertical
    localparam  VF = 10;     //Front porch vertical
    localparam  VR = 2;
    
    //0 si en rango 1 si n�, debido a que va a los reseet de los contadores del sprite y lso stretch
    assign OUT_xy_inrange    =   ~(IN_xSprite<=IN_xpos && IN_xSprite+(SW*Xstretch)>IN_xpos
                                && IN_ySprite<=IN_ypos && IN_ySprite+(SH*Ystretch)>IN_ypos);
                          
    
    assign OUT_moveSprite   =   (IN_ypos>=(VD+VF) && IN_ypos<(VD+VF+VR))?  1'b1:1'b0;
endmodule
