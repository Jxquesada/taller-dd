`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.05.2019 21:47:50
// Design Name: 
// Module Name: count
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module count(
    input logic Btnr,
    output logic [5:0]count2
    );
    integer count22=0;
     always@(posedge Btnr)
    begin
    if (count22==111111) count22=0;
    else count22=count22+1;
    end
     assign count2=count22;
endmodule
