`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 26.04.2019 20:05:54
// Design Name: 
// Module Name: Rotador_sprite
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Rotador_sprite(
    input   wire [3:0] IN_X,
    input   wire [3:0] IN_Y,
    input   wire [1:0] IN_Selec,
    output  wire [7:0] OUT_XYaddr
    );

assign OUT_XYaddr[3:0]=
    (IN_Selec==0)? IN_X:
    (IN_Selec==1)?~IN_Y:
    (IN_Selec==2)?~IN_X:
                   IN_Y;
                   
assign OUT_XYaddr[7:4]=
    (IN_Selec==0)? IN_Y:
    (IN_Selec==1)? IN_X:
    (IN_Selec==2)?~IN_Y:
                  ~IN_X;
//always_comb
//begin
//case(IN_Selec)
//0:      begin
//        OUT_XYaddr=IN_X;
//        OUT_XYaddr[7:4]<=IN_Y;
//        end
        
//1:      begin
//        OUT_XYaddr[3:0]<=IN_X;
//        OUT_XYaddr[7:4]<=IN_X;
//        end
    
//2:      begin
//        OUT_XYaddr[3:0]<=IN_X;
//        OUT_XYaddr[7:4]<=IN_X;
//        end
    
//3:      begin
//        OUT_XYaddr[3:0]=IN_X;
//        OUT_XYaddr[7:4]=IN_X;
//        end
//    endcase
// end
endmodule
