`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 27.04.2019 14:16:48
// Design Name: 
// Module Name: Animation_Sprite
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Animation_Sprite(
    input  wire IN_Clk,
    input  wire IN_Rst,
    input  wire [7:0] IN_Addres,
    output wire [5:0] OUT_Pixcolor,
    output wire OUT_Nullpix
    );
    
    localparam nNULL=1;
    
    logic switch=0;
    wire [5:0]Pixcolor,PixcolorStand;
    
    wire [4:0]OUT_count;
    
   
Counter_2Params #(/**/5,/**/30) Sprite_Switch(IN_Clk,IN_Rst,OUT_count);

ROM256x6 #(.Datafile("BombStep.mem")) bombwalk (IN_Addres,Pixcolor);
ROM256x6 #(.Datafile("BombStand.mem"))bombstand(IN_Addres,PixcolorStand);


assign OUT_Pixcolor=(switch)?PixcolorStand:Pixcolor;
assign OUT_Nullpix =(OUT_Pixcolor[1:0]==nNULL);
always@(posedge IN_Clk)
    begin
        if(OUT_count==30)
        switch=~switch;
    end
endmodule
