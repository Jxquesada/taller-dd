`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 26.04.2019 13:03:23
// Design Name: 
// Module Name: VGA_Controller_sprite_imp_test
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////



module VGA_Controller_sprite_imp_test(
    input   wire    clk100,
    output  wire    OUT_Hsync,
    output  wire    OUT_Vsync,
    output  wire    [11:0] OUT_RGB4bits
    );
    wire    [9:0] Xpos;
    wire    [9:0] Ypos;
    wire    VideoOn;
    logic [11:0] RGB;
    wire clk_25MHz;
    wire [7:0]YX;
    logic [5:0] SELECTRGB;
    
    
  Clock_divider#(2) clk25_1(clk100,0,clk_25MHz);
 
 VGA_Controller VGA_SyncData(clk_25MHz,OUT_Hsync,OUT_Vsync,Xpos,Ypos,VideoOn);
 
ROM256x6 #(.Datafile("BombStep.mem")) bombwalk (YX,SELECTRGB);
 Paleta_de_colores  #(.Datafile("Paleta.mem")) PaletaSpr(SELECTRGB,RGB);
  
    assign YX[3:0] = Xpos[3:0];
    assign YX[7:4] = Ypos[3:0];
    assign OUT_RGB4bits = VideoOn==1? RGB:12'b0;



endmodule
