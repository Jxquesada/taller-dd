`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Johan Chaves Zamora
// 
// Create Date: 28.04.2019 13:35:19
// Design Name: 
// Module Name: Set_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//Prueba del programa principal con 4 colores aleotarios pruebas y un reset
module Set_TB(
    );
   logic clk100=0; 
   logic IN_Btn=0;       
  logic IN_Reset=0;
  logic reset=0;     
  wire OUT_Vsync;
  wire OUT_Hsync;
  wire [11:0]OUT_RGB;   
  logic R,G,B ;     
  VGA_Sprite_SET  UT(.clk100(clk100),.IN_Btn(IN_Btn),.IN_Reset(IN_Reset),.reset(reset),.OUT_Hsync(OUT_Hsync),.OUT_Vsync(OUT_Vsync),.R(R),.G(G),.B(B),.OUT_RGB(OUT_RGB));
    always #10 clk100 = ~clk100;
    initial
    begin
    reset=1;
    #20 reset=0;
    #50000000 IN_Btn=1;
    #50000000 IN_Btn=0;
    #50000000 IN_Btn=1;
    #50000000 IN_Btn=0;
    #50000000 IN_Btn=1;
    #50000000 IN_Btn=0;    
    #50000000 
    IN_Btn=1;
    IN_Reset=1;
    #50000000
     IN_Btn=0;
     IN_Reset=0;
    #50000000 IN_Btn=1;
    #50000000 IN_Btn=0;   
    end
    
   
    
    
   
    
//    always 
//    begin
//        #400
//        IN_Btn=~IN_Btn&enable;
////        #105 
////        IN_Btn=~IN_Btn&enable;
////        #120 
////        IN_Btn=~IN_Btn&enable;
////        #200 
////        IN_Btn=~IN_Btn&enable;
////        #100 
////        IN_Btn=~IN_Btn&enable;
////        #100 
////        IN_Btn=~IN_Btn&enable;
////        #100 
////        IN_Btn=~IN_Btn&enable;
////        #100;
//        end;

//    initial
//        begin
//        #400
//        IN_Btn=~IN_Btn;
//        #105 
//        IN_Btn=~IN_Btn;
//        #120 
//        IN_Btn=~IN_Btn;
//        #200 
//        IN_Btn=~IN_Btn;
//        #100 
//        IN_Btn=~IN_Btn;
//        #100 
//        IN_Btn=~IN_Btn;
//        #100 
//        IN_Btn=~IN_Btn;
//        #100;
//        end
    
    
endmodule
