`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Johan chaves zamora
// 
// Create Date: 22.04.2019 18:23:48
// Design Name: 
// Module Name: vga_Test_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module vga_Test_TB_Paleta_LED(

    );
 //Pinta los 64 colores en la pantalla cada 8 pixeles en x , ademas tiene el led pwm controlado por los 12 switchs a 25Mhz cada pulso
logic   IN_Pixel_CLK=0;    
logic  [5:0] sw;  
logic reset;
wire    OUT_Hsync;         
wire    OUT_Vsync;        
wire    [11:0] OUT_RGB3bits;
wire R,G,B;
    
VGA_Controller_implement_TEST UUT(.clk100(IN_Pixel_CLK) ,.reset(reset),.sw(sw),.OUT_Hsync(OUT_Hsync),.OUT_Vsync(OUT_Vsync),.OUT_RGB(OUT_RGB3bits),.R(R),.G(G),.B(B));     
always #19.84 IN_Pixel_CLK=~IN_Pixel_CLK;
initial 
begin
 reset=1;
#10 reset=0;
sw=6'd3;
#5000 
sw=6'h4;
#5000 
sw=6'd15;
#5000 
sw=6'd61;
end
endmodule
