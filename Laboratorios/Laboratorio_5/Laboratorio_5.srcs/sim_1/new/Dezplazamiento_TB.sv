`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Johan Chaves Zamora
// 
// Create Date: 28.04.2019 16:31:43
// Design Name: 
// Module Name: Dezplazamiento_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// Prueba de dezplazamiento eje xy sprite
module Dezplazamiento_TB(

    );
    wire [1:0] IN_SquareState=0;
    logic IN_Move=0;
    wire [9:0] OUT_xSprite;
    wire [9:0] OUT_ySprite;
    wire [1:0] RotateState;
    
Sprite_movement #( /*Xstretch=*/2,/*Ystretch=*/2)UUT(IN_SquareState,IN_Move,OUT_xSprite,OUT_ySprite,RotateState);
always#10 IN_Move=~IN_Move;

    

endmodule
