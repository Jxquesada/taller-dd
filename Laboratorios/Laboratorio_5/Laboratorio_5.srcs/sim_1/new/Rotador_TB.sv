`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Johan Chaves Zamora
// 
// Create Date: 28.04.2019 17:14:07
// Design Name: 
// Module Name: Rotador_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// Prueba de Rotacion de sprite ejes x,y
module Rotador_TB(

    );
logic [3:0] IN_X=4'b001;    
logic [3:0] IN_Y=4'b0111; 
logic [1:0] IN_Selec=2'b0;
wire [7:0] OUT_XYaddr;
    
Rotador_sprite UUT(
   IN_X,
   IN_Y,
   IN_Selec,
   OUT_XYaddr
    );
    
    initial
    begin
    #10 IN_Selec=IN_Selec+1;
    #10 IN_Selec=IN_Selec+1;
    #10 IN_Selec=IN_Selec+1;
    #10 IN_Selec=IN_Selec+1;
    end
    
endmodule
