`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Johan Chaves Zamora
// 
// Create Date: 25.04.2019 23:33:03
// Design Name: 
// Module Name: SpriteBomb_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//Prueba de Carga de Sprite 
module SpriteBomb_TB(

    );
    logic [5:0] SELECTRGB;
     logic [7:0] IN_Addres=0;
     wire [11:0] OUT_Data;
    
ROM256x6 #(.Datafile("BombStand.mem"))bombstand(IN_Addres,SELECTRGB);
Paleta_de_colores  #(.Datafile("Paleta.mem")) PaletaSpr(SELECTRGB,OUT_Data);
always#5
IN_Addres=IN_Addres+1;

endmodule
