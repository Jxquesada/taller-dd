`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Johan chaves zamora
// 
// Create Date: 21.04.2019 13:56:36
// Design Name: 
// Module Name: VGA_controller_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//prueba de contolador VGA sincronizacion vertical y horizontal sin salida RGB
module VGA_Controller_TB(

    );
    
    logic    IN_Pixel_CLK=0;        
    wire    OUT_Hsync;       
    wire    OUT_Vsync;           
    wire    [9:0] OUT_Xpos;           
    wire    [9:0] OUT_Ypos;            
    wire    OUT_VideoOn;
    
VGA_Controller UUT(.IN_Pixel_CLK(IN_Pixel_CLK),.OUT_Hsync(OUT_Hsync),.OUT_Vsync(OUT_Vsync),.OUT_Xpos(OUT_Xpos),.OUT_Ypos(OUT_Ypos),.OUT_VideoOn(OUT_VideoOn));      
always #5 IN_Pixel_CLK=~IN_Pixel_CLK;
endmodule
