`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 27.04.2019 20:36:29
// Design Name: 
// Module Name: StretchTest
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module StretchTest_TB(



    );
    logic IN_clk=0;
    logic IN_Reset=1;
    logic Out_SquareSignal;

Squaresignal_byclockcounter #(2,3) UUT(.IN_clk(IN_clk),.IN_Reset(IN_Reset),.Out_SquareSignal(Out_SquareSignal));


initial
begin
#16 
IN_Reset=0;
#100;
IN_Reset=1;
end
always
#5 IN_clk=~IN_clk;
endmodule
